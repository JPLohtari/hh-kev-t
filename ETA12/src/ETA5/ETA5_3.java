package ETA5;

import java.util.Scanner;

public class ETA5_3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		String passCode = "ruska";
		Boolean correct = false;
		
		for(Integer i = 1; i < 4; i++)
		{
			if(correct)
			{
				i=4;
				continue;
			}
			
			System.out.print("Ovivahti kysyy " + i.toString() + ". kertaa, tunnussana? ");
			String input = scan.nextLine();
			
			if(input.equals(passCode))
				correct = true;
		}
		
		if(correct)
			System.out.println("SISÄÄN");
		else
			System.out.println("OVI ON LUKITTU");
	}

}
