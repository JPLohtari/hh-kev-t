package ETA5;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ETA5_1 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		System.out.print("Anna ostosten summa: ");
		String inputString = scan.nextLine();
		inputString = inputString.replace(',', '.');
		Double price = 0.0;
		Double factor = 0.0;
		try {
			price = Double.parseDouble(inputString);
		}
		catch(Exception e)
		{
			return;
		}
		
		System.out.print("Oletko kanta-asiakas (kyllä, ei): ");
		inputString = scan.nextLine();
		if(inputString.equals("kyllä") && price > 999.9)
			factor = 0.99;
		else if(inputString.equals("kyllä") && price < 1000)
			factor = 0.995;
		else
			factor = 1.0;
		
		scan.close();
		System.out.println("Ostosten loppusumma on " + dec.format(price * factor));
		
	}

}
