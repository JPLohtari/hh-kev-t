package ETA5;

import java.util.Scanner;

public class ETA5_5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Anna opiskelijatunnus: ");
		String input = scan.nextLine();
		if(validStuNum(input))
			System.out.println("Opiskelijatunnus on kelvollinen");
		else
			System.out.println("Opiskelijatunnus on kelvoton");
		
		input = "";

		System.out.print("Anna opiskelijan nimi: ");
		input = scan.nextLine();
		if(validStuName(input))
			System.out.println("Opiskelijan nimi on kelvollinen");
		else
			System.out.println("Opiskelijan nimi on kelvoton");
		
		input = "";
				
		System.out.print("Anna opiskelijan ryhmä: ");
		input = scan.nextLine();
		if(validStuGrp(input))
			System.out.println("Opiskelijan ryhmä on kelvollinen");
		else
			System.out.println("Opiskelijan ryhmä on kelvoton");
		
		input = "";
		System.out.print("Anna opiskelijan ikä: ");
		input = scan.nextLine();
		if(validStuAgeString(input))
		{
			if(validStuAge(input))
				System.out.println("Ikä on kelvollinen");
			else
				System.out.println("Ikä pitää olla 18-65");
		}
		else
			System.out.println("Ikä pitää olla numeerinen");
		
		scan.close();
	}

	public static Boolean validStuNum(String input)
	{
		return input.matches("a\\d{7}");
	}
	
	public static Boolean validStuGrp(String input)
	{
		return input.matches("[A-Z]{2}\\d{1}[A-Z]{2}");
	}	
	
	public static Boolean validStuName(String input)
	{
		return input.matches("[A-ZÄÅÖa-zäåö-]{1,}\\s{1,30}[A-ZÄÅÖa-zäåö+-]{1,}");
	}
	
	public static Boolean validStuAge(String input)
	{		
		int age = Integer.parseInt(input);
		if(age > 17 && age < 66)
			return true;
		else
			return false;
	}
	
	public static Boolean validStuAgeString(String input)
	{
		if(!input.matches("\\d{1,}"))
			return false;
		else
			return true;
	}
}
