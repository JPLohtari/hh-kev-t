package ETA5;

import java.util.ArrayList;
import java.util.Scanner;

public class ETA5_2 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		ArrayList<String> phrases = new ArrayList<String>();
		
		do
		{
			System.out.print("Anna sananlasku: ");
			String input = scan.nextLine();
			
			if(input.equals("LOPPU"))
				break;
			
			phrases.add(input);
		}
		while(true);
		scan.close();
		Integer chars = 0;
		if(!phrases.isEmpty())
		{
			for(int i = 0; i < phrases.size(); i++)
			{
				chars = chars + phrases.get(i).length();
			}
		}
		else
			return;
		
		System.out.println("Sananlaskuja oli " + ((Integer)phrases.size()).toString() + " kappaletta.");
		System.out.println("Sananlaskuissa oli merkkejä yhteensä " + chars.toString());
	}

}
