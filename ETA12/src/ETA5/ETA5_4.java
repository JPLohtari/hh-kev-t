package ETA5;

import java.util.Scanner;

public class ETA5_4 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Anna etu- ja sukunimi: ");
		String input = scan.nextLine();
		input = prepareString(input);
		String[] names = splitByWhiteSpaces(input);
		String nameString = "";
		String addressString = "";
		String postalOfficeString = "";
		for(int i = 0; i < names.length; i++)
		{		
			names[i] = names[i].substring(0, 1).toUpperCase() + names[i].substring(1);
			nameString = nameString + names[i];
			if(i < (names.length - 1))
					nameString = nameString + " ";
		}
		input = "";
		System.out.print("Anna katuosoite: ");
		input = scan.nextLine();
		input = prepareString(input);
		
		String[] addresses = splitByWhiteSpaces(input);
		addresses[0] = addresses[0].substring(0, 1).toUpperCase() + addresses[0].substring(1);
		
		for(int i = 0; i < addresses.length; i++)
		{		
			addressString = addressString + addresses[i];
			if(i < (addresses.length - 1))
					addressString = addressString + " ";
		}
		input = "";
		System.out.print("Anna postinumero ja postitoimipaikka: ");
		input = scan.nextLine();
		input = prepareString(input).toUpperCase();
		
		String[] offices = splitByWhiteSpaces(input);
		for(int i = 0; i < addresses.length; i++)
		{		
			postalOfficeString = postalOfficeString + offices[i];
			if(i < (addresses.length - 1))
					postalOfficeString = postalOfficeString + " ";
		}		
		
		System.out.println(nameString);
		System.out.println(addressString);;
		System.out.println(postalOfficeString);
		
	}
	
	private static String[] splitByWhiteSpaces(String input)
	{
		return input.replaceAll("\\s{2,}", " ").split(" ");
	}
	
	private static String prepareString(String input)
	{
		return input.trim().toLowerCase();
	}

}
