package ETA12;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ETA12_1 {

	public static void main(String[] args) {
		System.out.println("Syötä neljän opintojakson tiedot!\n");
		Map<String,Opintojakso> jaksot = new HashMap<String,Opintojakso>();
		for(int i=0; i < 4; i++)
		{
			Opintojakso jakso = getOpintojaksoData();
			jaksot.put(jakso.getTunnus(), jakso);
		}
		System.out.println("Opintojaksot: ");
		Set<String> tunnukset = jaksot.keySet();
		Iterator<String> i = tunnukset.iterator();
		while(i.hasNext())
		{
			String tunnus = i.next();
			Opintojakso jakso = jaksot.get(tunnus);
			
			System.out.println(tunnus + ": " + jakso.getNimi() + jakso.getLaajuus().toString() + " opintopistettä");
		}
		
		
		
	}

	private static Opintojakso getOpintojaksoData()
	{
		Scanner scan = new Scanner(System.in);
		Opintojakso uusi = new Opintojakso();
		System.out.print("Anna opintojakson tunnus: ");
		uusi.setTunnus(scan.nextLine());
		System.out.print("Anna opintojakson nimi: ");
		uusi.setNimi(scan.nextLine());
		System.out.print("Anna opintojakson laajuus: ");
		uusi.setLaajuus(scan.nextInt());
		scan.nextLine();
		
		return uusi;
		
	}
}

class Opintojakso
{
	String tunnus;
	String nimi;
	Integer laajuus;
	
	Opintojakso()
	{
		
	}
	
	public String getTunnus() {
		return tunnus;
	}

	public void setTunnus(String tunnus) {
		this.tunnus = tunnus;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public Integer getLaajuus() {
		return laajuus;
	}

	public void setLaajuus(Integer laajuus) {
		this.laajuus = laajuus;
	}
	
	public String toString()
	{
		return "tunnus="+this.tunnus+", nimi="+this.nimi+", laajuus="+laajuus.toString();
	}

}