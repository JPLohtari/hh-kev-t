package ETA12;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ETA12_3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		Integer paino = 0;
		Double pituus = 0.0;
		boolean painoOK = false;
		boolean pituusOK = false;
		while(!painoOK)
		{
			try{
				paino = kysyPaino(scan);
				painoOK = true;
			}
			catch(InputMismatchException e)
			{
				System.out.println("Paino pitää olla kokonaisluku!");
			}
			catch(KelvotonPainoPoikkeus k)
			{
				System.out.println("Painon pitää olla > 0");
			}
			catch(Exception x)
			{
			}
		}
		
		while(!pituusOK) 
		{
			try
			{
				pituus = kysyPituus(scan);
				pituusOK = true;
			}
			catch(InputMismatchException i)
			{
				System.out.println("Pituus pitää olla desimaaliluku!");
			}
			catch(KelvotonPituusPoikkeus k)
			{
				System.out.println("Ihmisen pituus pitää olla > 0 ja alle 3 metriä");
			}
			catch(Exception e)
			{
			}
		}
		
		Double bmi = paino/(pituus * pituus);
		System.out.println("Painokerroin on " + dec.format(bmi) + " (kun paino on " + paino.toString() + "kg ja pituus on " + dec.format(pituus) + " metrinä)");
		
		

	}
	
	private static Integer kysyPaino(Scanner scan) throws InputMismatchException, KelvotonPainoPoikkeus
	{
		System.out.print("Anna paino (kg): ");
		String input = scan.nextLine();
		if(!input.matches("\\d*"))
			throw new InputMismatchException("paino kokonaisluku");
		input = input.replace(',', '.');
		Double check = 0.0;
		check = Double.parseDouble(input);
		if((check - check.intValue()) != 0)
			throw new InputMismatchException("Painon tulee olla positiivinen kokonaisluku!");
		Integer paino = Integer.parseInt(input); 
		if(paino <= 0) 
			throw new KelvotonPainoPoikkeus("Painon tulee olla positiivinen kokonaisluku!");
		
		return paino;
	}
	
	private static Double kysyPituus(Scanner scan) throws InputMismatchException, KelvotonPituusPoikkeus
	{
		System.out.print("Anna pituus (m): ");
		Double pituusInput = 0.0;
		String input = scan.nextLine();
		if(input.matches("^\\d{1},\\d{1,2}$"))
		{
			input = input.replace(",",".");
			pituusInput = Double.parseDouble(input);
			if(pituusInput <=0.0 || pituusInput > 3.0)
				throw new KelvotonPituusPoikkeus("Pituus huono");
			
			return pituusInput;
		}
		else if(input.matches("^\\d*$"))
			throw new KelvotonPituusPoikkeus("Pituus huono");
		else
			throw new InputMismatchException("Pituus huono");
			
	}

}

class KelvotonPainoPoikkeus extends Exception {
	  public KelvotonPainoPoikkeus(String viesti) {
	    super(viesti);
	   }
	}
class KelvotonPituusPoikkeus extends Exception {
	    public KelvotonPituusPoikkeus(String viesti) {
	      super(viesti);
	    }

	}
