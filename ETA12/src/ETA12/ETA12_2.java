package ETA12;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ETA12_2 {

	public static void main(String[] args) {

		List<Integer> numbers = new ArrayList<Integer>();
		Scanner scan = new Scanner(System.in);
		
		for(int i = 1; i < 11; i++)
		{
			System.out.print("Anna " + i + ". luku: ");
			String input = scan.nextLine();
			boolean ok = false;
			Integer answer = 0;
			try {
				answer = Integer.parseInt(input);
				ok = true;
			}
			catch(Exception e)
			{
				System.out.println("Syötä kokonaisluku numeronäppäimillä!");
				ok = false;
			}
			
			if(ok)
				numbers.add(answer);
			else
				i--;
			
		}
		System.out.println("LUVUT:");
		for(int i = 0; i < numbers.size(); i++)
		{
			System.out.print(numbers.get(i) + " ");
		}
	}

}
