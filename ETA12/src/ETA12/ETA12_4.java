package ETA12;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ETA12_4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Map<String,Ystava> ystavat = new HashMap<String,Ystava>();
		do {
			System.out.println("1) Lisää ystävä");
			System.out.println("2) Etsi ystävä");
			System.out.println("3) Poista ystävä");
			System.out.println("4) Tulosta ystävät");
			System.out.println("0) Lopeta");
			System.out.print("Valitse: ");
			String input = scan.nextLine();
			
			if(input.equals("0"))
				break;
			
			if(input.equals("1"))
			{
				Ystava newFriend = addNewFriend(scan, ystavat);
				if(!newFriend.getLempinimi().equals(""))
					ystavat.put(newFriend.getLempinimi(), newFriend);
			}	
			
			if(input.equals("2"))
			{
				System.out.print("Anna ystävän lempinimi: ");
				String name = scan.nextLine();
				findFromDb(name, ystavat);
			}	
			if(input.equals("3"))
			{
				System.out.print("Anna ystävän lempinimi: ");
				String name = scan.nextLine();
				removeFromDb(name, ystavat);
			}
			if(input.equals("4"))
			{
				printAllFromDb(ystavat);
			}


		}while(true);

	}
	
	private static Boolean checkIfAlreadyInDb(String lempinimi, Map<String,Ystava> ystavat)
	{
		Set<String> keys = ystavat.keySet();
		boolean returnValue = false;
		Iterator<String> i = keys.iterator();
		while(i.hasNext())
		{
			Ystava friend = ystavat.get(i.next());
			String nameInDb = friend.getLempinimi();
			if(nameInDb.equals(lempinimi))
			{
				returnValue = true;
				System.out.println("Olet jo lisännyt tämän ystävän\nYstävän tiedot: "+friend.toString());
				
			}
		}
		
		return returnValue;
		
	}
	
	private static void findFromDb(String lempinimi, Map<String,Ystava> ystavat)
	{
		Set<String> keys = ystavat.keySet();
		Iterator i = keys.iterator();
		while(i.hasNext())
		{
			Ystava friend = ystavat.get(i.next());
			if(friend.getLempinimi().equals(lempinimi))
			{
				System.out.println("Ystävän tiedot: ");
				System.out.println(friend.toString());
				return;
			}
		}
		
		System.out.println("Ystävää, jonka lempinimi on "+lempinimi+" ei ole");
	}
	
	private static void removeFromDb(String lempinimi, Map<String,Ystava> ystavat)
	{
		Set<String> keys = ystavat.keySet();
		Iterator i = keys.iterator();
		while(i.hasNext())
		{
			Ystava friend = ystavat.get(i.next());
			if(friend.getLempinimi().equals(lempinimi))
			{
				System.out.println("Poistetaan ystävän tiedot: ");
				System.out.println(friend.toString());
				ystavat.remove(friend.getLempinimi());
				return;
			}
		}
		
		System.out.println("Ystävää, jonka lempinimi on "+lempinimi+" ei ole");
	}

	
	private static Ystava addNewFriend(Scanner scan, Map<String,Ystava> ystavat)
	{
		Ystava friend = new Ystava();
		System.out.print("Anna ystävän lempinimi: ");
		friend.setLempinimi(scan.nextLine());
		boolean alreadyAdded = checkIfAlreadyInDb(friend.getLempinimi(), ystavat);
		if(alreadyAdded)
		{
			return new Ystava();
		}
		System.out.print("Anna ystävän nimi: ");
		friend.setNimi(scan.nextLine());
		System.out.print("Anna ystävän puhelinnumero: ");
		friend.setPuhelin(scan.nextLine());
		String pvm = "";
		boolean dayOk = false;
		while(!dayOk)
		{
			System.out.print("Anna ystävän syntymäpäivämäärä muodossa pp.kk.vvvv: ");
			pvm = scan.nextLine();
			
			if(pvm.matches("\\d{1,2}.\\d{1,2}.\\d{4}"))
			{
				dayOk = true;
			}
			else
				System.out.println("Väärä muoto! Muodon tulee olla pp.kk.vvvv!");
		}
		friend.setSyntpvm(pvm);
		
		return friend;
	}
	
	private static void printAllFromDb(Map<String, Ystava> ystavat)
	{
		Set<String> keys = ystavat.keySet();
		Iterator i = keys.iterator();
		while(i.hasNext())
		{
			System.out.println(ystavat.get(i.next()).toString());
		}
	}

}

class Ystava
{
	private String lempinimi;
	private String nimi;
	private String puhelin;
	private Date syntpvm;
	
	public Ystava()
	{
		lempinimi = "";
		nimi = "";
		puhelin = "";
		syntpvm = new Date();
	}
	
	public String getLempinimi() {
		return lempinimi;
	}
	public void setLempinimi(String lempinimi) {
		this.lempinimi = lempinimi;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getPuhelin() {
		return puhelin;
	}
	public void setPuhelin(String puhelin) {
		this.puhelin = puhelin;
	}
	public Date getSyntpvm() {
		return syntpvm;
	}
	public void setSyntpvm(String syntpvm) {
		SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");
		Date pvm = null;
		try {
			pvm = parser.parse(syntpvm);
		} catch (ParseException e) {
			pvm = null;
		}
		
		if(pvm != null)
			this.syntpvm = pvm;
	}
	
	public String toString()
	{
		SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");
		String dateString = parser.format(syntpvm);
		return "lempinimi="+this.lempinimi+", nimi="+this.nimi+", puhelin="+this.puhelin+", syntpvm="+dateString;
	}
	
}