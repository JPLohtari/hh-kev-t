package tehtava5;

import java.util.ArrayList;

public class Elaintarha {

	public static void main(String[] args) {
		ArrayList<Object> elukat = new ArrayList<Object>();
		
		elukat.add(new Leijona());
		elukat.add(new Leijona());
		elukat.add(new Paviaani());
		elukat.add(new Hevonen());

		kuuntele(elukat);
	}
	
	public static void kuuntele(ArrayList<Object> elukat)
	{
		for(int i = 0; i < elukat.size(); i++)
		{
			String luokka = elukat.get(i).getClass().getSimpleName();
			if(luokka.equals("Leijona"))
				System.out.println(new Leijona().puhu());
			else if(luokka.equals("Paviaani"))
				System.out.println(new Paviaani().puhu());
			else if(luokka.equals("Hevonen"))
				System.out.println(new Hevonen().puhu());
		}
	}

}
