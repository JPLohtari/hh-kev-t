package ETA11;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ETA11_3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		List<Henkilo> henkilot = new ArrayList<Henkilo>();
		
		do {
			System.out.println("1. Lisää henkilö");
			System.out.println("2. Näytä henkilön tiedot");
			System.out.println("3. Muuta henkilön nimi ja osoite");
			System.out.println("4. Muuta henkilön koko");
			System.out.println("5. Näytä kaikki henkilöt");
			System.out.println("0. Lopetus");
			System.out.print("Anna valintasi (0-5): ");
			String input = scan.nextLine();
			int menuSel = -1;
			try {
				menuSel = Integer.parseInt(input);
			}
			catch(Exception e)
			{
				menuSel = -1;
			}
			
			if(menuSel == 0)
				break;
			
			switch(menuSel) {
				case 1:
					henkilot.add(addNewPerson(scan));
					break;
				case 2:
					System.out.print("Anna näytettävän henkilön nimi: ");
					getPersonByName(scan.nextLine(),henkilot);
					break;
				case 3:
					System.out.print("Anna perustietoja muutettavan henkilön nimi: ");
					changePersonBasicInfo(scan.nextLine(), henkilot, scan);
					break;
				case 4:
					System.out.print("Anna kokoa muutettavan henkilön nimi: ");
					changePersonMetricInfo(scan.nextLine(), henkilot, scan);
					break;
				case 5:
					showAllPersons(henkilot);
					break;
					
				default:
					break;
			}
	
			

		} while(true);
		
	}
	private static Henkilo addNewPerson(Scanner scan)
	{
		Henkilo person = new Henkilo();
		System.out.print("Anna nimi: ");
		person.setName(scan.nextLine());
		System.out.print("Anna osoite: ");
		person.setAddr(scan.nextLine());
		System.out.print("Anna pituus: ");
		person.getKoko().setHeight(scan.nextDouble());
		scan.nextLine();
		System.out.print("Anna paino: ");
		person.getKoko().setWeight(scan.nextDouble());
		scan.nextLine();		
		return person;
	}
	
	private static void getPersonByName(String name, List<Henkilo> henkilot)
	{
		Henkilo found = null;
		for(int i = 0; i < henkilot.size(); i++)
		{
			if(henkilot.get(i).getName().equals(name))
				found = henkilot.get(i);
		}
		
		if(found == null)
			System.out.println("Henkilöä ei ole");
		else
		{
			System.out.println("\nNimi: " + found.getName());
			System.out.println("Osoite: " + found.getAddr());
			System.out.println("Pituus: " + found.getKoko().getHeightString());
			System.out.println("Painoindeksi: " + found.getKoko().getBmiString() + "\n");
		}
	}
	
	private static void changePersonBasicInfo(String name, List<Henkilo> henkilot, Scanner scan)
	{
		Henkilo found = null;
		int foundInt = -1;
		for(int i = 0; i < henkilot.size(); i++)
		{
			if(henkilot.get(i).getName().equals(name))
			{
				found = henkilot.get(i);
				foundInt = i;
			}
		}
		
		if(found == null)
			System.out.println("Henkilöä ei ole");
		else
		{
			System.out.print("Anna nimi: ");
			found.setName(scan.nextLine());
			System.out.print("Anna osoite: ");
			found.setAddr(scan.nextLine());
			henkilot.set(foundInt, found);
		}
		
	}
	
	private static void changePersonMetricInfo(String name, List<Henkilo> henkilot, Scanner scan)
	{
		Henkilo found = null;
		int foundInt = -1;
		for(int i = 0; i < henkilot.size(); i++)
		{
			if(henkilot.get(i).getName().equals(name))
			{
				found = henkilot.get(i);
				foundInt = i;
			}
		}
		
		if(found == null)
			System.out.println("Henkilöä ei ole");
		else
		{
			System.out.print("Anna pituus: ");
			found.getKoko().setHeight(scan.nextDouble());
			scan.nextLine();
			System.out.print("Anna paino: ");
			found.getKoko().setWeight(scan.nextDouble());
			scan.nextLine();
			henkilot.set(foundInt, found);
		}
		
	}
	
	private static void showAllPersons(List<Henkilo> henkilot)
	{
		for(int i = 0; i < henkilot.size(); i++)
		{
			System.out.println("\nNimi: " + henkilot.get(i).getName());
			System.out.println("Osoite: " + henkilot.get(i).getAddr());
			System.out.println("Pituus: " + henkilot.get(i).getKoko().getHeightString());
			System.out.println("Painoindeksi: " + henkilot.get(i).getKoko().getBmiString() + "\n");

		}
	}
	
}
class Henkilo {
	
	private String name;
	private String addr;
	private Koko koko;
	
	Henkilo() {
		koko = new Koko();
	}
	
	Henkilo(String name, String addr)
	{
		this.name = name;
		this.addr = addr;
	}
	Henkilo(String name, String addr, Koko koko)
	{
		this.name = name;
		this.addr = addr;
		this.koko = koko;
	}
	
	public String toString()
	{
		return "nimi="+this.name+", osoite="+this.addr+", bmi= "+koko.getBmi().toString();
	}
	
	public String getAddr() 
	{
		return this.addr;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddr(String addr)
	{
		this.addr = addr;
	}

	public Koko getKoko() {
		return koko;
	}

	public void setKoko(Koko koko) {
		this.koko = koko;
	}
	
}

class Koko
{
	private Double height;
	private Double weight;

	Koko() {}
	Koko(Double height, Double weight)
	{
		this.setHeight(height);
		this.setWeight(weight);
	}
	public Double getWeight() {
		return weight;
	}
	
	public String getWeightString() {
		DecimalFormat dec = new DecimalFormat("0.00");
		return dec.format(this.weight);
	}
	
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getHeight() {
		return height;
	}
	
	public String getHeightString() {
		DecimalFormat dec = new DecimalFormat("0.00");
		return dec.format(this.height);
	}
	
	public void setHeight(Double height) {
		this.height = height;
	}
	
	public Double getBmi() {
		return weight/(height*height);
	}
	
	public String getBmiString() {
		DecimalFormat dec = new DecimalFormat("0.00");
		return dec.format(this.getBmi());
	}
	
}