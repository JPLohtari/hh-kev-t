package ETA11;

import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
// Sanalistasovellus
public class ETA11_1 {

	private List<String> sanaLista = new ArrayList<String>();

	private void lisaaSana(String input) {
		sanaLista.add(input);
	}

	private void listaaSanat() {
		String returnString = "";
		for(int i = 0; i < sanaLista.size(); i++)
		{
			if(i > 0 || i == (sanaLista.size() - 1))
				returnString = returnString + " ";
			
			returnString = returnString + sanaLista.get(i);
		}
		System.out.println(returnString);
	}

	private void laskeSanat(String input) {
		Integer occur = 0;
		for(int i = 0; i < sanaLista.size(); i++)
		{
			if(sanaLista.get(i).equals(input))
				occur++;
		}
		System.out.println("Sana " + input + " esiintyi " + occur.toString() + " kertaa");

	}

	private void poistaSana(String input) {
		int occur = 0;
		for(int i = 0; i < sanaLista.size(); i++)
		{
			if(sanaLista.get(i).equals(input))
			{
				occur++;
				sanaLista.remove(i);
			}
		}		
		if(occur == 0)
			System.out.println("Sanaa " + input + " ei ollut listassa");
		else
			System.out.println("Jokainen sana " + input + " on poistettu listasta");
	}

	public static void main(String[] args) {
		ETA11_1 ohjelma = new ETA11_1();

		int valinta;

		Scanner input = new Scanner(System.in);

		do {
			System.out.println("\n1 = Lisää sana");
			System.out.println("2 = Listaa sanat");
			System.out.println("3 = Laske sanat");
			System.out.println("4 = Poista sanat");
			System.out.println("0 = Lopeta");
			System.out.print("Anna valintasi: ");

			valinta = input.nextInt();
			input.reset();

			switch (valinta) {
			case 1:
				System.out.print("Anna sana: ");
				ohjelma.lisaaSana(input.next());
				break;

			case 2:
				ohjelma.listaaSanat();
				break;

			case 3:
				System.out.print("Anna sana, minkä esiintymiskerrat lasketaan: ");
				ohjelma.laskeSanat(input.next());
				break;

			case 4:
				System.out.print("Anna sana, joka poistetaan: ");
				ohjelma.poistaSana(input.next());
				break;
			}
		} while (valinta != 0);
	}
}