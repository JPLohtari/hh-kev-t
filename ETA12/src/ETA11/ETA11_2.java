package ETA11;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
// Pelaajalistasovellus
public class ETA11_2 {

	public static void main(String[] args) {
		ETA11_2 sovellus = new ETA11_2();
		List<Pelaaja> pelaajaLista = new ArrayList<Pelaaja>();
		int valinta = -1;
		Scanner input = new Scanner(System.in);

		do {
			System.out.println("\n1. Lisää pelaaja");
			System.out.println("2. Hae pelaaja");
			System.out.println("3. Näytä pelaajat");
			System.out.println("0. Lopetus");

			System.out.print("Anna valintasi (0-3): ");
			String valintaString = input.next();
			input.nextLine();
			
			try {
				valinta = Integer.parseInt(valintaString);
			}
			catch(Exception e)
			{
				valinta = -1;
			}

			switch (valinta) {
			case 1:
				System.out.print("Anna pelinumero: ");
				int gamenum = input.nextInt();
				input.nextLine();
				System.out.print("Anna nimi: ");
				String plrName = input.nextLine();
				System.out.print("Anna pelipaikka: ");
				String plrSpot = input.nextLine();
				
				Pelaaja plr = new Pelaaja(gamenum,plrName,plrSpot);
				pelaajaLista.add(plr);				
				break;
			case 2:
				System.out.println("Anna pelinumero: ");
				int plrNum = input.nextInt();
				printPlayerInfo(plrNum, pelaajaLista);
				break;
			case 3:
				printPlayers(pelaajaLista);
				break;
			case 0:
				break;
			default:
				System.out.println("Virheellinen valinta");
			}
		} while (valinta != 0);
	}
	
	private static void printPlayerInfo(Integer plrNum, List<Pelaaja> pelaajaLista)
	{
		try 
		{
			Pelaaja plr = pelaajaLista.get(plrNum-1); // Since player 1 is actually player 0..
			System.out.println("\nPelinumero: " + plr.getPelinumero());
			System.out.println("Nimi: " + plr.getNimi());
			System.out.println("Pelipaikka: " + plr.getPelipaikka());
		}
		catch(Exception e)
		{
			System.out.println("Pelaaja ei ole pelinumerolla " + plrNum.toString());
		}
	}
	
	private static void printPlayers(List<Pelaaja> pelaajaLista)
	{
		for(int i = 1; i < pelaajaLista.size() + 1; i++) // Since player 1 is actually player 0..
		{
			printPlayerInfo(i,pelaajaLista);
			System.out.println(" ");
		}
	}
}



class Pelaaja {

	private int pelinumero;
	private String nimi;
	private String pelipaikka;

	public Pelaaja() {
		pelinumero = 0;
		nimi = "";
		pelipaikka = "";
	}

	public Pelaaja(int pelinumero, String nimi, String pelipaikka) {
		this.pelinumero = pelinumero;
		this.nimi = nimi;
		this.pelipaikka = pelipaikka;
	}

	public int getPelinumero() {
		return pelinumero;
	}

	public void setPelinumero(int pelinumero) {
		this.pelinumero = pelinumero;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public String getPelipaikka() {
		return pelipaikka;
	}

	public void setPelipaikka(String pelipaikka) {
		this.pelipaikka = pelipaikka;
	}

	public String toString() {
		return "pelinumero=" + pelinumero + ", nimi=" + nimi + ", pelipaikka="
				+ pelipaikka;
	}

}