package ETA4;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ETA4_3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		Double sum = 0.0;
		Double vahennys = (double)0;
		
		System.out.print("Syötä 0 lopettaksesi\nSyötä RESET nollataksesi\n");
		do {
			System.out.print("Anna työkorvauksen määrä (0 lopettaa): ");
			String inputString = scan.nextLine();
			inputString = inputString.replace(',', '.');
			inputString = inputString.trim();

			if(inputString.equals("0"))
			{
				break;
			}
			else if(inputString.equals("RESET"))
			{
				sum = 0.0;
				continue;
			}
				
			Double vahsum = 0.0;
			try {
				vahsum = Double.parseDouble(inputString);
			}
			catch(Exception e)
			{
				System.out.println("Malfunction.");
			}
			
			sum = sum+vahsum;
			
		} while(true);
		scan.close();
		
		if(sum > 0)
			vahennys = sum*(45.0/100.0)-100.0;
		
		if(vahennys > 2400)
			vahennys = (double)2400;
		
		if(vahennys < 0)
			vahennys = 0.0;
		
		
		System.out.println("Kotitalousvähennyksen määrä on "+dec.format(vahennys)+" euroa");
		
	}

}