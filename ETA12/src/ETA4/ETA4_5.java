package ETA4;

import java.util.Random;
import java.util.Scanner;

public class ETA4_5 {
	
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Integer guesses = 0;
		Random rnd = new Random();
		// Integer okNum = rnd.nextInt(100)+1; 
		// But we can't use this. Only for testings sake :)
		Integer okNum = 78;
		
		System.out.print("Arvaa lukua väliltä 1-100: ");
		do 
		{
			guesses++;
			Integer guess = scan.nextInt();
			
			if(guess == okNum)
				break;
			else if (guess > okNum)
			{
				System.out.print("Arvaa pienempää: ");
			}
			else if(guess < okNum)
			{
				System.out.print("Arvaa suurempaa: ");
			}
			
		}
		while(true);
		scan.close();
		System.out.println("Arvasit oikein. Arvauksia oli " + guesses.toString());
		
		
	}

}