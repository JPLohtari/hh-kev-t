package ETA4;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ETA4_1 {
// Also known as Kassakone
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		Boolean continuity = true;
		Double sum = 0.0;
		
		System.out.print("Syötä 0 lopettaksesi\nSyötä RESET nollataksesi\n");
		do {
			System.out.print("Anna ostoksen hinta (0 lopettaa): ");
			String inputString = scan.nextLine();
			inputString = inputString.replace(',', '.');

			if(inputString.equals("0"))
			{
				continuity = false;
				break;
			}
			else if(inputString.equals("RESET"))
			{
				sum = 0.0;
				inputString = "0.0";
				System.out.println("SUM Reset");
			}
			
			Double price = 0.0;
			try {
				price = Double.parseDouble(inputString);
			}
			catch(Exception e)
			{
				price = 0.0;
				System.out.println("Malfunction.");
			}
			
			sum = sum+price;
			
		}
		while(continuity);

		if(sum > 0)
		{
			Double alv = (sum*24/124);
			System.out.println("Ostosten verollinen hinta on " + dec.format(sum));
			System.out.println("ALV:n osuus on " + dec.format(alv));
			System.out.println("Veroton hinta on " + dec.format(sum - alv));
		}
		if(sum == 0)
			System.out.println("Goodbye");
		
	}

}