package ETA4;

import java.text.DecimalFormat;
import java.util.Scanner;
public class ETA4_2 {


	// Also known as Kilometrikorvauslaskuri
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		Boolean continuity = true;
		Double sum = 0.0;
		Double reimpursementRate = 0.43;
		
		System.out.print("Syötä 0 lopettaksesi\nSyötä RESET nollataksesi\n");
		do {
			System.out.print("Anna ajetut kilometrit (0 lopettaa): ");
			String inputString = scan.nextLine();
			inputString = inputString.replace(',', '.');
			inputString = inputString.trim();

			if(inputString.equals("0"))
			{
				continuity = false;
				break;
			}
			else if(inputString.equals("RESET"))
			{
				sum = 0.0;
				inputString = "0.0";
				System.out.println("SUM Reset");
			}
			
			Double travelDistance = 0.0;
			try {
				travelDistance = Double.parseDouble(inputString);
			}
			catch(Exception e)
			{
				travelDistance = 0.0;
				System.out.println("Malfunction.");
			}
			
			sum = sum+travelDistance;
			
		}
		while(continuity);

		if(sum > 0)
		{
			Double totalReimpursement = (sum*reimpursementRate);
			System.out.println("Yhteensä " + sum.intValue() + " kilometriä");
			System.out.println("Korvaus on " + dec.format(totalReimpursement) + "euroa");
		}
		if(sum == 0)
			System.out.println("Goodbye");
		
	}

}
