package ETA4;

import java.util.Scanner;

public class ETA4_4 {
	
	public static void main(String[] args)
{
	Scanner scan = new Scanner(System.in);
	Integer sum = 0;
	do
	{
		System.out.print("Rikesakon määrä: ");
		String input = scan.nextLine();
		input = input.trim();
		Integer fine = 0;
		
		if(input.equals("0"))
			break;
		
		try
		{
			fine = Integer.parseInt(input);
		}
		catch(NumberFormatException e)
		{
			fine = 0;
		}
		
		if(isGood(fine))
			sum = sum + fine;
		else
			System.out.println("Virheellinen sakon määrä");
		
	} while(true);
	System.out.println("Rikesakkojen summa on " + sum.toString());
}

private static Boolean isGood(Integer fine)
{
	if(fine == 70)
		return true;
	else if(fine == 85)
		return true;
	else if(fine == 100)
		return true;
	else if(fine == 115)
		return true;
	else
		return false;
	
}
}
