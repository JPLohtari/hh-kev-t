package ETA9;

import java.util.Scanner;

public class ETA9_3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Kirja kirja = null;
		Kustantaja kustantaja = null;
		do {
		System.out.println("1 = Luo/Muuta kustantaja");
		System.out.println("2 = Luo/Muuta kirja");
		System.out.println("3 = Lisää kirjalle kustantaja");
		System.out.println("4 = Näytä kirjan tiedot");
		System.out.println("0 = Lopetus");
		System.out.print("Anna valintasi: ");
		String input = scan.nextLine();
		
		if(input.equals("0"))
			break;
		
		if(input.equals("1"))
		{
			System.out.print("Anna kustantajan nimi: ");
			String kustantajaNimi = scan.nextLine();
			System.out.print("Anna kustantajan osoite: ");
			String kustantajaOsoite = scan.nextLine();
			System.out.print("Anna kustantajan puhelin: ");
			String kustantajaPuh = scan.nextLine();
			
			kustantaja = new Kustantaja(kustantajaNimi,kustantajaOsoite,kustantajaPuh);
		}
		
		if(input.equals("2"))
		{
			System.out.print("Anna kirjan nimi: ");
			String kirjaNimi = scan.nextLine();
			System.out.print("Anna kirjan isbn: ");
			String kirjaIsbn = scan.nextLine();
			System.out.print("Anna kirjan hinta: ");
			String kirjaHintaString = scan.nextLine();
			Double kirjaHinta = 0.0;
			try {
				kirjaHintaString = kirjaHintaString.replace(',', '.');
				kirjaHinta = Double.parseDouble(kirjaHintaString);
			}
			catch(Exception e)
			{
				
			}
			kirja = new Kirja(kirjaNimi,kirjaIsbn,kirjaHinta,null);
		}
		
		if(input.equals("3"))
		{
			if(kustantaja != null && kirja != null)
				kirja.setKustantaja(kustantaja);
		}
		
		if(input.equals("4"))
		{
			if(kirja != null)
			{
				System.out.println("Nimi: " + kirja.getNimi());
				System.out.println("Isbn: " + kirja.getIsbn());
				System.out.println("Hinta: " + kirja.getHinta().toString());
				if(kirja.getKustantaja() != null)
					System.out.println("Kustantaja: " + kirja.getKustantaja().getNimi());
				else
					System.out.println("Kustantaja: ");
			}
		}
		
		
		
		
		}while(true);
		
	}

}
class Kirja {
	
	private String nimi;
	private String isbn;
	private Double hinta;
	private Kustantaja kustantaja;
	
	public Kirja()
	{
		
	}
	
	public Kirja(String nimi, String isbn, Double hinta, Kustantaja kustantaja)
	{
		this.nimi = nimi;
		this.isbn = isbn;
		this.hinta = hinta;
		this.kustantaja = kustantaja;
	}
	
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public Double getHinta() {
		return hinta;
	}
	public void setHinta(Double hinta) {
		this.hinta = hinta;
	}

	public Kustantaja getKustantaja() {
		return kustantaja;
	}

	public void setKustantaja(Kustantaja kustantaja) {
		this.kustantaja = kustantaja;
	}
	
}

class Kustantaja
{
	private String nimi;
	private String osoite;
	private String puhelin;
	
	public Kustantaja()
	{}
	
	public Kustantaja(String nimi, String osoite, String puhelin)
	{
		this.nimi = nimi;
		this.osoite = osoite;
		this.puhelin = puhelin;
	}
	
	
	public String getOsoite() {
		return osoite;
	}
	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getPuhelin() {
		return puhelin;
	}
	public void setPuhelin(String puhelin) {
		this.puhelin = puhelin;
	}
	
	
}