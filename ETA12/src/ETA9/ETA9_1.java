package ETA9;

import java.util.Date;
import java.util.Scanner;

public class ETA9_1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Anna merkki: ");
		String carMake = scan.nextLine();
		System.out.print("Anna malli: ");
		String carModel = scan.nextLine();
		System.out.print("Anna rekisterinumero: ");
		String regnum = scan.nextLine();
		System.out.print("Anna vuosimalli: ");
		String year = scan.nextLine();
		System.out.print("Anna nimi: ");
		String name = scan.nextLine();
		System.out.print("Anna osoite: ");
		String addr = scan.nextLine();
		
		Auto car = new Auto();
		Henkilo person = new Henkilo();
		person.setAddr(addr);
		person.setName(name);
		car.setMake(carMake);
		car.setModel(carModel);
		car.setRegNum(regnum);
		car.setYear(year);
		person.setCar(car);
		
		
		System.out.println("Nimi: "+person.getName());
		System.out.println("Osoite: "+person.getAddr());
		System.out.println("Auto:"+person.getCar().getMake() + " " + person.getCar().getModel());
		

	}

}

class Auto 
{
	private String make;
	private String model;
	private String regNum;
	private Integer year;
	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRegNum() {
		return regNum;
	}
	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}
	public Integer getYear() {
		return year;
	}
	public String getYearString() {
		return year.toString();
	}
	public void setYear(String year) {
		this.year = Integer.parseInt(year);
	}
	
	public Integer getCarAge() 
	{
		Date curDate = new Date();
		Integer curYear = curDate.getYear()+1900;
		return (curYear - this.year);
		
	}
}

class Henkilo {
	
	private String name;
	private String addr;
	private Auto car;
	
	Henkilo() {
		
	}
	
	Henkilo(String name, String addr)
	{
		this.name = name;
		this.addr = addr;
		this.car = null;
	}
	
	Henkilo(String name, String addr, Auto car)
	{
		this.name = name;
		this.addr = addr;
		this.car = car;
	}
	
	public String toString()
	{
		return "nimi="+this.name+", osoite="+this.addr+" auto="+car.getMake()+" "+car.getModel();
	}
	
	public String getAddr() 
	{
		return this.addr;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddr(String addr)
	{
		this.addr = addr;
	}
	
	public Auto getCar() {
		return car;
	}
	public void setCar(Auto car) {
		this.car = car;
	}
	
}