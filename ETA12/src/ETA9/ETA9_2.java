package ETA9;

import java.util.Scanner;

public class ETA9_2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Kirjan nimi:");
		String nimi = scan.nextLine();
		System.out.print("Kirjan isbn:");
		String isbn = scan.nextLine();
		System.out.print("Kirjan hinta:");
		String hintaString = scan.nextLine();
		Double hinta = 0.0;
		try {
			hinta = Double.parseDouble(hintaString);
		}
		catch(Exception e)
		{
			hinta = 0.0;
		}
		
		System.out.print("Kustantajan nimi:");
		String kustantajanimi = scan.nextLine();
		System.out.print("Kustantajan osoite:");
		String kustantajaosoite = scan.nextLine();
		System.out.print("Kustantajan puhelin:");
		String kustantajapuhelin = scan.nextLine();
		
		Kustantaja kustantaja = new Kustantaja(kustantajanimi,kustantajaosoite,kustantajapuhelin);
		Kirja kirja = new Kirja(nimi,isbn,hinta,kustantaja);
		
		System.out.println("Kirjan " + kirja.getNimi() + " kustantaja on " + kirja.getKustantaja().getNimi());

	}

}

class Kirja {
	
	private String nimi;
	private String isbn;
	private Double hinta;
	private Kustantaja kustantaja;
	
	public Kirja()
	{
		
	}
	
	public Kirja(String nimi, String isbn, Double hinta, Kustantaja kustantaja)
	{
		this.nimi = nimi;
		this.isbn = isbn;
		this.hinta = hinta;
		this.kustantaja = kustantaja;
	}
	
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public Double getHinta() {
		return hinta;
	}
	public void setHinta(Double hinta) {
		this.hinta = hinta;
	}

	public Kustantaja getKustantaja() {
		return kustantaja;
	}

	public void setKustantaja(Kustantaja kustantaja) {
		this.kustantaja = kustantaja;
	}
	
}

class Kustantaja
{
	private String nimi;
	private String osoite;
	private String puhelin;
	
	public Kustantaja()
	{}
	
	public Kustantaja(String nimi, String osoite, String puhelin)
	{
		this.nimi = nimi;
		this.osoite = osoite;
		this.puhelin = puhelin;
	}
	
	
	public String getOsoite() {
		return osoite;
	}
	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getPuhelin() {
		return puhelin;
	}
	public void setPuhelin(String puhelin) {
		this.puhelin = puhelin;
	}
	
	
}
