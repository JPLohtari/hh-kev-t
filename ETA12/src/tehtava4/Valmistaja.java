package tehtava4;

public class Valmistaja {
	private String yTunnus;
	private String nimi;
	private String osoite;
	private String puhelin;
	
	public Valmistaja()
	{
		super();
	}

	public Valmistaja(String yTunnus, String nimi, String osoite, String puhelin) {
		super();
		this.yTunnus = yTunnus;
		this.nimi = nimi;
		this.osoite = osoite;
		this.puhelin = puhelin;
	}
	
	public String getyTunnus() {
		return yTunnus;
	}
	public void setyTunnus(String yTunnus) {
		this.yTunnus = yTunnus;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getOsoite() {
		return osoite;
	}
	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}
	public String getPuhelin() {
		return puhelin;
	}
	public void setPuhelin(String puhelin) {
		this.puhelin = puhelin;
	}
	@Override
	public String toString() {
		return "Valmistaja [yTunnus=" + yTunnus + ", nimi=" + nimi
				+ ", osoite=" + osoite + ", puhelin=" + puhelin + "]";
	}
	
	
	
}
