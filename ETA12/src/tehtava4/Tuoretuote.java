package tehtava4;

public class Tuoretuote extends Tuote {
	private String viimeinenPVM;

	public Tuoretuote(String viimeinenPVM) {
		super();
		this.viimeinenPVM = viimeinenPVM;
	}

	public Tuoretuote(String viimeinenPVM, String tuotenumero, String nimi, double hinta, Valmistaja valmistaja)
	{
		super();
		this.viimeinenPVM = viimeinenPVM;
		this.setTuotenumero(tuotenumero);
		this.setNimi(nimi);
		this.setHinta(hinta);
		this.setValmistaja(valmistaja);
	}

	
	public String getViimeinenPVM() {
		return viimeinenPVM;
	}

	public void setViimeinenPVM(String viimeinenPVM) {
		this.viimeinenPVM = viimeinenPVM;
	}

	@Override
	public String toString() {
		return "Tuoretuote [viimeinenPVM=" + viimeinenPVM + "]";
	}
	
	

}
