package tehtava4;

import java.text.DecimalFormat;

public class Kauppa {

	public static void main(String[] args) {
		DecimalFormat dec = new DecimalFormat("0.0");
		Valmistaja valmistaja1 = new Valmistaja("201010-2","Paavon banaanit", "Banaanikuja 3, Hollola", "050-1111111");
		Valmistaja valmistaja2 = new Valmistaja("212134-2","Tomin junat", "Junatie 3, Myrskylä", "050-3232323");
		Lelu lelu = new Lelu(3,"032323","Junavaunu",43.2,valmistaja2);
		Tuoretuote banaani = new Tuoretuote("20.03.2016","10","Banaani",3.2,valmistaja1);
		
		System.out.println("Lelun tiedot");
		System.out.println("Tuotenumero: " + lelu.getTuotenumero());
		System.out.println("Nimi: " + lelu.getNimi());
		System.out.println("Hinta: " + dec.format(lelu.getHinta()).replace(",", ".") + " euroa");
		System.out.println("Ikäsuositus: " + lelu.getIkasuositus() + " vuotta");
		System.out.println("Valmistajan nimi: " + lelu.getValmistaja().getNimi());
		
		System.out.println("\n\nTuoretuotteen tiedot");
		System.out.println("Tuotenumero: " + banaani.getTuotenumero());
		System.out.println("Nimi: " + banaani.getNimi());
		System.out.println("Hinta: " + dec.format(banaani.getHinta()).replace(",", ".") + " euroa");
		System.out.println("Viimeinen käyttöpäivämäärä: " + banaani.getViimeinenPVM());
		System.out.println("Valmistajan nimi: " + banaani.getValmistaja().getNimi());
		
		
		


	}

}
