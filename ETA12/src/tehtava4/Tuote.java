package tehtava4;

public class Tuote {
	private String tuotenumero;
	private String nimi;
	private Double hinta;
	private Valmistaja valmistaja;
	
	public Tuote()
	{
		super();
	}
	
	public Tuote(String tuotenumero, String nimi, Double hinta,
			Valmistaja valmistaja) {
		super();
		this.tuotenumero = tuotenumero;
		this.nimi = nimi;
		this.hinta = hinta;
		this.valmistaja = valmistaja;
	}
	
	public String getTuotenumero() {
		return tuotenumero;
	}
	public void setTuotenumero(String tuotenumero) {
		this.tuotenumero = tuotenumero;
	}
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public Double getHinta() {
		return hinta;
	}
	public void setHinta(Double hinta) {
		this.hinta = hinta;
	}
	public Valmistaja getValmistaja() {
		return valmistaja;
	}
	public void setValmistaja(Valmistaja valmistaja) {
		this.valmistaja = valmistaja;
	}
	@Override
	public String toString() {
		return "Tuote [tuotenumero=" + tuotenumero + ", nimi=" + nimi
				+ ", hinta=" + hinta + ", valmistaja=" + valmistaja + "]";
	}
	
	

}
