package tehtava4;

public class Lelu extends Tuote {
	private int ikasuositus;
	
	public Lelu()
	{
		super();
	}

	public Lelu(int ikasuositus) {
		super();
		this.ikasuositus = ikasuositus;
	}
	
	public Lelu(int ika, String tuotenumero, String nimi, double hinta, Valmistaja valmistaja)
	{
		super();
		this.ikasuositus = ika;
		this.setTuotenumero(tuotenumero);
		this.setNimi(nimi);
		this.setHinta(hinta);
		this.setValmistaja(valmistaja);
	}

	public int getIkasuositus() {
		return ikasuositus;
	}

	public void setIkasuositus(int ikasuositus) {
		this.ikasuositus = ikasuositus;
	}

	@Override
	public String toString() {
		return "Lelu [ikasuositus=" + ikasuositus + "]";
	}
	
	
}
