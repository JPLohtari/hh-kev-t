package ETA3;
import java.text.DecimalFormat;
import java.util.Scanner;


public class ETA3_1 {
// Also known as Palkka
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		DecimalFormat decimals = new DecimalFormat("0.00");
		
		System.out.print("Anna palkka: ");
		Double salary = input.nextDouble();
		System.out.print("Anna veroprosentti: ");
		Double taxRate = input.nextDouble();
		System.out.print("Anna ikä: ");
		Integer age = input.nextInt();
		input.close();
	
		Double tev = tevCalc(age);
		Double taxPart = salary * (taxRate/100);
		Double tevPart = salary * (tev);
		Double ueiPart = salary * 0.0115; // TEHTÄVÄNANNOSSA KAKSI VAIHTOEHTOA!
		
		System.out.println("Bruttopalkka " + salary.intValue());
		System.out.println("Veron osuus " + decimals.format(taxPart));
		System.out.println("Työeläkevakuutusmaksun osuus " + decimals.format(tevPart));
		System.out.println("Työttömyysvakuutuksen osuus " + decimals.format(ueiPart));
		System.out.println("Käteen jää " + decimals.format(salary-taxPart-tevPart-ueiPart));
				
	}
	
	private static double tevCalc(int age)
	{
		if(age < 53)
			return (5.55/100);
		else
			return (7.05/100);
	}

}
