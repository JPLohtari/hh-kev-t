package ETA3;

import java.util.Scanner;

public class ETA3_5 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Anna nopeusrajoitus: ");
		Integer speedLimit = scan.nextInt();
		System.out.print("Anna nopeutesi: ");
		Integer speedDone = scan.nextInt();
		
		scan.close();
		Integer fine = 0;
		if(speedDone < speedLimit || speedDone == speedLimit || speedLimit == 0)
		{
			System.out.println("Mene pois.");
			return;
		}
		Integer speedDiff = speedDone - speedLimit;
		
		if(speedLimit < 61)
		{
			if(speedDiff < 15)
				fine = 170;
			else if(speedDiff > 15 && speedDiff < 21)
				fine = 200;
			else
				fine = 300;
		}
		else if(speedLimit > 61)
		{
			if(speedDiff < 15)
				fine = 140;
			else if(speedDiff > 15 && speedDiff < 21)
				fine = 200;
			else
				fine = 300;
		}
		else
			return;
		
		if(!fine.equals(300))
			System.out.println("Rikesakko on " + fine.toString() );
		else
			System.out.println("Menee päiväsakoille");
		
	}

}
