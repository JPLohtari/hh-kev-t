package ETA3;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ETA3_4 {
//Also known as pizza
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		
		System.out.print("Anna pizzojen hinnat (3): ");
		String input = scan.nextLine();
		input = input.replace(',', '.');
		scan.close();
		
		Double sum = 0.0;

		String[] inputs = input.split(" ");
		ArrayList<Double> prices = new ArrayList<Double>();
		for(int i = 0; i < inputs.length; i++)
		{
			Double price = Double.parseDouble(inputs[i]);
			prices.add(price);
		}
		Collections.sort(prices);
				
		for(int i = 0; i < prices.size(); i++)
		{
			sum = sum + prices.get(i);
		}
		
		sum = sum - prices.get(0);
		System.out.println("Maksettavaa: " + dec.format(sum));
		System.out.println("Yksittäisen hinta: " + dec.format(sum / prices.size()));
	}

}