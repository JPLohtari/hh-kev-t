package ETA3;

import java.text.DecimalFormat;

import java.util.Scanner;

public class ETA3_2 {
//Also known as ylevero
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		Double yleTax = 0.0;
		
		System.out.print("Vuositulot ja ikä? ");
		String inputString = scan.nextLine();
		String[] inputs = inputString.split(" ");
		inputs[0] = inputs[0].replace(',', '.');
		
		Double income;
		Integer age;
		try {
			income = Double.parseDouble(inputs[0]);
			age = Integer.valueOf(inputs[1]);
		}
		catch(Exception e)
		{
			income = 0.0;
			age = 0;
		}

		scan.close();

		if((income < 7353) ||(age < 18))
		{
			System.out.println("Ylevero: " + dec.format(yleTax));
			return;
		}
		
		yleTax = income * (0.68/100);
		if(yleTax > 140)
			yleTax = 140.0;
		else if(yleTax < 70)
			yleTax = 0.0;
		
		System.out.println("Ylevero: " + dec.format(yleTax));
			}

}
