package ETA3;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ETA3_3 {
// Also known as matkalaskuri
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		System.out.print("Montako matkaa teet kuukaudessa: ");
		Integer times = scan.nextInt();
		System.out.print("Anna yksittäisen lipun hinta: ");
		Double singleTrip = scan.nextDouble();
		System.out.print("Anna kuukausilipun hinta: ");
		Double monthPass = scan.nextDouble();
		
		scan.close();
		
		if((singleTrip * times) > monthPass)
		{
			System.out.print("Kuukausilippu on " + dec.format((singleTrip*times)-monthPass) + " euroa halvempi kuin yksittäinen");
		}
		else if(monthPass > (singleTrip * times))
		{
			System.out.print("Yksittäinen on " + dec.format(monthPass - (singleTrip*times))+ " euroa halvempi kuin kuukausilippu");
		}
		else
		{
			System.out.print("Aivan sama kumman otat");
		}
		
	}

}