package ETA13;

import java.util.Scanner;

public class ETA13_1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Tyontekija tyontekija = new Tyontekija();
		System.out.println("Syötä työntekijan tiedot!");
		System.out.print("Anna nimi: ");
		tyontekija.setNimi(scan.nextLine());
		System.out.print("Anna osoite:");
		tyontekija.setOsoite(scan.nextLine());
		System.out.print("Anna tuntipalkka: ");
		tyontekija.setTuntipalkka(scan.nextDouble());
		scan.nextLine();
		
		System.out.println("TYÖNTEKIJÄTIEDOT");
		System.out.println("Nimi: " + tyontekija.getNimi());
		System.out.println("Osoite: " + tyontekija.getOsoite());
		System.out.println("Tuntipalkka: " + tyontekija.getTuntipalkka());
		
	}

}


class Henkilo
{
	private String nimi;
	private String osoite;
	
	public Henkilo()
	{
		
	}
	
	public Henkilo(String nimi, String osoite)
	{
		this.nimi = nimi;
		this.osoite = osoite;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public String getOsoite() {
		return osoite;
	}

	public void setOsoite(String osoite) {
		this.osoite = osoite;
	}
	
}

class Tyontekija extends Henkilo
{
	private double tuntipalkka;
	
	Tyontekija()
	{
		super();
	}
	
	Tyontekija(String nimi, String osoite, double tuntipalkka)
	{
		super(nimi,osoite);
		this.setTuntipalkka(tuntipalkka);
	}

	public double getTuntipalkka() {
		return tuntipalkka;
	}

	public void setTuntipalkka(double tuntipalkka) {
		this.tuntipalkka = tuntipalkka;
	}
	
}