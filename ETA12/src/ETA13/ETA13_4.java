package ETA13;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ETA13_4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		ArrayList<Opiskelija> opiskelijat = new ArrayList<Opiskelija>();
		while(true)
		{
			System.out.println("1. Lisää opiskelija");
			System.out.println("2. Näytä kaikki opiskelijat");
			System.out.println("3. Näytä opiskelija");
			System.out.println("4. Lisää suoritus");
			System.out.println("0. Lopetus");
			System.out.print("Anna valintasi(0-4): ");
			String input = scan.nextLine();
			
			if(input.equals("0"))
				break;
			else if(input.equals("1"))
			{
				// Add
				lisaaOpiskelija(opiskelijat,scan);
			}
			else if(input.equals("2"))
			{
				// Show all
				naytaOpiskelijat(opiskelijat,scan);
			}
			else if(input.equals("3"))
			{
				// Show one
				naytaOpiskelija(opiskelijat,scan);
			}
			else if(input.equals("4"))
			{
				// Add suoritus
				lisaaSuoritus(opiskelijat,scan);
			}
			else
			{
				System.out.println(" ");
			}
			
		}
		scan.close();
		
	}
	
	private static boolean opiskelijaOlemassa(ArrayList<Opiskelija> opiskelijat, String opiskelijanumero)
	{
		for(int i = 0; i < opiskelijat.size(); i++)
		{
			if(opiskelijat.get(i).getOpiskelijanumero().equals(opiskelijanumero))
				return true;
		}
		return false;
	}
	
	private static Opiskelija getOpiskelija(ArrayList<Opiskelija> opiskelijat, String opsnum)
	{
		Opiskelija opiskelija = null;
		for(int i = 0; i < opiskelijat.size(); i++)
		{
			if(opiskelijat.get(i).getOpiskelijanumero().equals(opsnum))
				opiskelija = opiskelijat.get(i);
		}
		
		return opiskelija;
	}
	
	private static void noSuch(String opsnum)
	{
		System.out.println("Opiskelijaa ei ole numerolla "+opsnum);
	}
	

	
	private static void lisaaOpiskelija(ArrayList<Opiskelija> opiskelijat, Scanner scan)
	{
		String opsnum = kysyOpiskelijanumero(scan);
		System.out.print("Anna nimi: ");
		String name = scan.nextLine();
		Opiskelija opiskelija = new Opiskelija(opsnum,name);
		opiskelijat.add(opiskelija);
	}
	
	private static void naytaOpiskelija(ArrayList<Opiskelija> opiskelijat, Scanner scan)
	{
		String opsnum = kysyOpiskelijanumero(scan);
		if(!opiskelijaOlemassa(opiskelijat,opsnum))
		{
			noSuch(opsnum);
			return;
		}
		
		Opiskelija opiskelija = getOpiskelija(opiskelijat,opsnum);
		
		if(opiskelija == null)
		{
			noSuch(opsnum);
			return;
		}
		
		System.out.println("Opiskelijanumero: "+opiskelija.getOpiskelijanumero());
		System.out.println("Nimi: "+opiskelija.getNimi());
		System.out.println("Suoritukset:");
		for(int i = 0; i < opiskelija.getSuoritukset().size(); i++)
		{
			System.out.println(opiskelija.getSuoritukset().get(i).toString());
		}			
	}
	
	private static void naytaOpiskelijat(ArrayList<Opiskelija> opiskelijat, Scanner scan)
	{		
		for(int i = 0; i < opiskelijat.size(); i++)
		{
			System.out.println(opiskelijat.get(i).toString());
			System.out.println("Suoritukset:");
			for(int x = 0; x < opiskelijat.get(i).getSuoritukset().size(); x++)
			{
				System.out.println(opiskelijat.get(i).getSuoritukset().get(x).toString());
			}
				
		}
	}
	
	private static void lisaaSuoritus(ArrayList<Opiskelija> opiskelijat, Scanner scan)
	{
		String opsnum = kysyOpiskelijanumero(scan);
		if(!opiskelijaOlemassa(opiskelijat,opsnum))
		{
			noSuch(opsnum);
			return;
		}
		
		Opiskelija opiskelija = getOpiskelija(opiskelijat,opsnum);
			
		System.out.print("Anna kurssitunnus: ");
		String kurstun = scan.nextLine();
		System.out.print("Anna arvosana: ");
		int kursarv = scan.nextInt();
		scan.nextLine();
		
		Suoritus suoritus = new Suoritus();
		suoritus.setKurssitunnus(kurstun);
		suoritus.setArvosana(kursarv);
		
		while(true)
		{
			System.out.print("Anna suorituspäivä (pp.kk.vvvv): ");
			String pvmString = scan.nextLine();
			try{
				suoritus.setSuorituspaiva(pvmString);
			}
			catch(Exception e)
			{
				System.out.println("Väärä formaatti!");
			}
			
			break;

		}

		opiskelija.getSuoritukset().add(suoritus);
	}
	
	private static String kysyOpiskelijanumero(Scanner scan)
	{
		System.out.print("Anna opiskelijanumero: ");
		String opsnum = scan.nextLine();
		return opsnum;
	}

}

class Opiskelija
{
	private String opiskelijanumero;
	private String nimi;
	private List<Suoritus> suoritukset;
	
	Opiskelija()
	{
		this.suoritukset = new ArrayList<Suoritus>();
	}


	public Opiskelija(String opiskelijanumero, String nimi) {
		super();
		this.opiskelijanumero = opiskelijanumero;
		this.nimi = nimi;
		this.suoritukset = new ArrayList<Suoritus>();

	}
	
	public Opiskelija(String opiskelijanumero, String nimi, ArrayList<Suoritus> suoritukset) {
		super();
		this.opiskelijanumero = opiskelijanumero;
		this.nimi = nimi;
		this.suoritukset = suoritukset;

	}

	public String getOpiskelijanumero() {
		return opiskelijanumero;
	}

	public void setOpiskelijanumero(String opiskelijanumero) {
		this.opiskelijanumero = opiskelijanumero;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public List<Suoritus> getSuoritukset() {
		return suoritukset;
	}

	public void setSuoritukset(List<Suoritus> suoritukset) {
		this.suoritukset = suoritukset;
	}
	
	public String toString()
	{
		return "Opiskelijanumero: "+this.opiskelijanumero+"\nNimi: "+this.nimi;
	}
}

class Suoritus
{
	private String kurssitunnus;
	private int arvosana;
	private Date suorituspaiva;
	
	Suoritus()
	{
		
	}

	public Suoritus(String kurssitunnus, int arvosana, Date suorituspaiva) {
		super();
		this.kurssitunnus = kurssitunnus;
		this.arvosana = arvosana;
		this.suorituspaiva = suorituspaiva;
	}
	
	public String getKurssitunnus() {
		return kurssitunnus;
	}

	public void setKurssitunnus(String kurssitunnus) {
		this.kurssitunnus = kurssitunnus;
	}

	public int getArvosana() {
		return arvosana;
	}

	public void setArvosana(int arvosana) {
		this.arvosana = arvosana;
	}

	public Date getSuorituspaiva() {
		return suorituspaiva;
	}

	public void setSuorituspaivaAsDate(Date suorituspaiva) {
		this.suorituspaiva = suorituspaiva;
	}
	
	public void setSuorituspaiva(String date) throws PaivaPoikkeus
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		Date pvm = new Date();
		try {
			pvm = sdf.parse(date);
		}
		catch(Exception e)
		{
			throw new PaivaPoikkeus("Paiva sekosi!");
		}
		this.suorituspaiva = pvm;
	}
	
	public String toString()
	{
		return this.kurssitunnus+" "+this.arvosana;
	}
	
}

class PaivaPoikkeus extends Exception
{
	PaivaPoikkeus(String viesti)
	{
		super("Paiva sekosi!");
	}
}