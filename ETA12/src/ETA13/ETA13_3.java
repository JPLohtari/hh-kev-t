package ETA13;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ETA13_3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Korttipakka pakka = new Korttipakka();
		while(true)
		{
			System.out.print("Anna pelikortin maa(-lopettaa): ");
			String maa = scan.nextLine();
			if(maa.equals("-"))
				break;
			
			System.out.print("Anna pelikortin numero: ");
			int numero = scan.nextInt();
			scan.nextLine();
			
			pakka.lisaa(new Pelikortti(maa,numero));		
		}
		
		
		System.out.println("Korttipakassa on "+pakka.annaKoko()+" korttia:");
		int pakkaindex = pakka.annaKoko();
		for(int i = pakkaindex ; i >= 0; i--)
		{
			System.out.println(pakka.anna(i).toString());
		}
	}

}


interface Korttipino
{
	void lisaa(Pelikortti pelikortti);
	Pelikortti poista();
	int annaKoko();
	Pelikortti anna(int index);
}

class Korttipakka implements Korttipino
{
	List<Pelikortti> korttipakka;	
	
	public Korttipakka()
	{
		korttipakka = new ArrayList<Pelikortti>();
	}
	
	public Korttipakka(ArrayList<Pelikortti> pakka)
	{
		this.korttipakka = pakka;
	}
	
	public Korttipakka(List<Pelikortti> korttipakka) {
		super();
		this.korttipakka = korttipakka;
	}
	
	

	public List<Pelikortti> getKorttipakka() {
		return korttipakka;
	}

	public void setKorttipakka(List<Pelikortti> korttipakka) {
		this.korttipakka = korttipakka;
	}

	@Override
	public void lisaa(Pelikortti pelikortti) {
		korttipakka.add(pelikortti);
	}

	@Override
	public Pelikortti poista() {
		// Tehtävänanto ei anna tälle funktiota?
		return null;
	}

	@Override
	public int annaKoko() {
		// TODO Auto-generated method stub
		return korttipakka.size();
	}

	@Override
	public Pelikortti anna(int index) {
		// TODO Auto-generated method stub
		return korttipakka.get(index);
	}
	
}

class Pelikortti
{
	private String maa;
	private int numero;
	
	Pelikortti()
	{
		
	}

	public Pelikortti(String maa, int numero) {
		this.maa = maa;
		this.numero = numero;
	}

	public String getMaa() {
		return maa;
	}

	public void setMaa(String maa) {
		this.maa = maa;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String toString()
	{
		return maa + " " + numero;
	}
	
	
}