package ETA13;

import java.util.Scanner;

public class ETA13_2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		KirjaTuote kirja = new KirjaTuote();
		DVDTuote dvd = new DVDTuote();
		System.out.println("Syötä kirjatuotteen tiedot!");
		System.out.print("Anna tuotekoodi:");
		kirja.setTuotekoodi(scan.nextLine());
		System.out.print("Anna nimi: ");
		kirja.setNimi(scan.nextLine());
		System.out.print("Anna hinta:");
		kirja.setHinta(scan.nextDouble());
		scan.nextLine();
		
		System.out.print("Anna sivumäärä: ");
		kirja.setSivumaara(scan.nextInt());
		scan.nextLine();
		System.out.print("Anna sidosasu: ");
		kirja.setSidosasu(scan.nextLine());
		System.out.println(" ");
		System.out.println("Syötä dvdtuotteen tiedot!");
		System.out.print("Anna tuotekoodi:");
		dvd.setTuotekoodi(scan.nextLine());
		System.out.print("Anna nimi: ");
		dvd.setNimi(scan.nextLine());
		System.out.print("Anna hinta:");
		dvd.setHinta(scan.nextDouble());
		scan.nextLine();
		System.out.print("Anna kesto(min): ");
		dvd.setKesto(scan.nextInt());
		scan.nextLine();
		System.out.print("Anna ikäsuositus: ");
		dvd.setIkasuositus(scan.nextLine());
		System.out.println("\nKIRJATUOTTEEN TIEDOT:");
		System.out.println("Tuotekoodi: " + kirja.getTuotekoodi());
		System.out.println("Nimi: " + kirja.getNimi());
		System.out.println("Hinta: " + kirja.getHinta());
		System.out.println("Sivumäärä: " + kirja.getSivumaara());
		System.out.println("Sidosasu: " + kirja.getSidosasu());
		System.out.println("\nDVDTUOTTEEN TIEDOT:");
		System.out.println("Tuotekoodi: " + dvd.getTuotekoodi());
		System.out.println("Nimi: " + dvd.getNimi());
		System.out.println("Hinta: " + dvd.getHinta());
		System.out.println("Kesto(min): " + dvd.getKesto());
		System.out.println("Ikäsuositus: " + dvd.getIkasuositus());

		
		
		
		
		
	}

}

class Tuote
{
	private String tuotekoodi;
	private String nimi;
	private double hinta;
	
	Tuote()
	{
		
	}

	Tuote(String tuotekoodi, String nimi, double hinta) {
		this.tuotekoodi = tuotekoodi;
		this.nimi = nimi;
		this.hinta = hinta;
	}

	public String getTuotekoodi() {
		return tuotekoodi;
	}

	public void setTuotekoodi(String tuotekoodi) {
		this.tuotekoodi = tuotekoodi;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public double getHinta() {
		return hinta;
	}

	public void setHinta(double hinta) {
		this.hinta = hinta;
	}
	
	
}

class KirjaTuote extends Tuote
{
	private int sivumaara;
	private String sidosasu;
	
	KirjaTuote()
	{
		super();
	}

	public KirjaTuote(String tuotekoodi, String nimi, double hinta, int sivumaara, String sidosasu) {
		super(tuotekoodi, nimi, hinta);
		this.sivumaara = sivumaara;
		this.sidosasu = sidosasu;
	}

	public int getSivumaara() {
		return sivumaara;
	}

	public void setSivumaara(int sivumaara) {
		this.sivumaara = sivumaara;
	}

	public String getSidosasu() {
		return sidosasu;
	}

	public void setSidosasu(String sidosasu) {
		this.sidosasu = sidosasu;
	}
}

class DVDTuote extends Tuote
{
	private int kesto;
	private String ikasuositus;
	
	DVDTuote()
	{
		super();
	}

	public DVDTuote(String tuotekoodi, String nimi, double hinta, int kesto, String ikasuositus) {
		super(tuotekoodi, nimi, hinta);
		this.kesto = kesto;
		this.ikasuositus = ikasuositus;
	}

	public int getKesto() {
		return kesto;
	}

	public void setKesto(int kesto) {
		this.kesto = kesto;
	}

	public String getIkasuositus() {
		return ikasuositus;
	}

	public void setIkasuositus(String ikasuositus) {
		this.ikasuositus = ikasuositus;
	}
	
	
}