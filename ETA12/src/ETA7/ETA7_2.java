package ETA7;
import java.text.DecimalFormat;
import java.util.Scanner;
public class ETA7_2 {

	public static void main(String[] args) {
		
		naytakulutus(laskeKulutus(kysyKilometrit(),kysyTankkaus()));
	}
	
	private static double laskeKulutus(int kilometrit, int tankattu)
	{
		return ((double)tankattu/(double)kilometrit)*100.0;
	}
	
	private static void naytakulutus(double kulutus)
	{
		DecimalFormat dec = new DecimalFormat("0.00");
		System.out.println("Kulutus/100km on " + dec.format(kulutus) + " litraa");
	}

	private static int kysyKilometrit()
	{
		Scanner scan = new Scanner(System.in);
		System.out.print("Anna ajetut kilometrit: ");
		int km = scan.nextInt();
		return km;
	}
	
	private static int kysyTankkaus()
	{
		System.out.print("Anna tankattu määrä: ");
		Scanner scan = new Scanner(System.in);
		int fuel = scan.nextInt();
		scan.close();
		return fuel;
	}

}
