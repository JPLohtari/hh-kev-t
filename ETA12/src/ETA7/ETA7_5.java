package ETA7;
import java.util.ArrayList;
import java.util.Scanner;
public class ETA7_5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		ArrayList<String> db = new ArrayList<String>();
		for(int i = 0; i < 1000; i++)
		{
			System.out.print("Anna veriryhmä (- lopettaa): ");
			String input = scan.nextLine();
			if(input.equals("-"))
			{
				i = 2000;
				break;
			}
			db.add(input.toUpperCase());
		}
		Integer dbSize = db.size();
		System.out.println("Luovutuksia oli yhteensä " + dbSize.toString());
		System.out.print("Minkä veriryhmän luovutusten määrän haluat tietää: ");
		String searchString = scan.nextLine();
		scan.close();
		Integer results = searchDB(db,searchString.toUpperCase());
		System.out.println("Veriryhmän " + searchString + " luovutuksia oli " + results.toString());

	}

	private static Integer searchDB(ArrayList<String> db, String searchString)
	{
		int amt = 0;
		for(int i = 0; i < db.size(); i++)
		{
			if(db.get(i).equals(searchString))
				amt++;
		}
		return amt;
	}
}
