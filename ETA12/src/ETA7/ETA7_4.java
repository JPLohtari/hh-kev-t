package ETA7;
import java.util.Arrays;
import java.util.Scanner;
public class ETA7_4 {

	public static void main(String[] args) {

		int MAX = 100;
		int[] lampotilat = new int[MAX];
		int numOfTemps = kysyLampotilat(lampotilat,MAX);
		naytaLampotilat(lampotilat, numOfTemps);
	}


	public static int kysyLampotilat(int[] lampotilat, final int max)
	{
		Scanner scan = new Scanner(System.in);
		int amt = 0;
		for(int i = 0; i < max; i++)
		{
			System.out.print("Anna lämpötila: ");
			int temp = scan.nextInt();
			if(temp == -999)
			{
				i = max+1;
				continue;
			}
			
			amt++;
			lampotilat[i] = temp;
		}
		return amt;
		
	}
	
	public static void naytaLampotilat(int[] lampotilat, int lkm)
	{
		Integer[] tempsFinal = new Integer[lkm];
		for(int i = 0; i < lkm; i++)
		{
			tempsFinal[i] = lampotilat[i];
		} // This is useless monkey business.
		Arrays.sort(tempsFinal);
		String temps = "";
		for(int i = 0; i < lkm; i++)
		{
			temps = temps + tempsFinal[i].toString();
			if(i != lkm-1)
				temps = temps + " ";
		}
		System.out.println("Annoit lämpötilat: " + temps);
	}
}
