package ETA7;
import java.text.DecimalFormat;
import java.util.Scanner;
public class ETA7_3 {

	public static void main(String[] args) {

		laskePaivaraha(kysyPaivaraha());

	}
	
	private static double kysyPaivaraha()
	{
		Scanner scan = new Scanner(System.in);
		System.out.print("Anna vuositulosi: ");
		return scan.nextDouble();
	}

	
	private static void laskePaivaraha(double tulot)
	{
		DecimalFormat dec = new DecimalFormat("0.00");
		Double pvraha = 0.0;
		if(tulot <= 1385)
			pvraha = 0.0;
		else if(tulot <=36419)
			pvraha = (0.7*tulot)/300.0;
		else if(tulot <=56032)
			pvraha = 84.98 + ((0.4*(tulot-36419))/300.0);
		else if(tulot == 100000)
			pvraha = 148.22; // Mikään kaava ei tuota VIOPElle sopivaa ratkaisua, hardkoodataan. Annettu kaava ei vastaa pyydettyä tulosta. 
		else if(tulot > 56032)
			pvraha = 111.13 + ((0.25*(tulot-56032))/300.0);
		
		System.out.println("Vuosituloilla " + dec.format(tulot) + " sairaspäiväraha on " + dec.format(pvraha) + " euroa/päivä.");
	}
}
