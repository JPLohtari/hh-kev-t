package ETA7;
import java.text.DecimalFormat;
import java.util.Scanner;
public class ETA7_1 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		System.out.print("Anna käynnin kesto: ");
		Integer length = scan.nextInt();
		System.out.print("Kestoltaan "+length.toString() + " minuutin yleislääkärikäynnistä kelakorvaus on " + dec.format(laskeKorvaus(length)) + " euroa");
		scan.close();

	}
	
	private static double laskeKorvaus(int kesto)
	{
		if(kesto <=10)
			return 8.00;
		else if(kesto <=20)
			return 11.00;
		else if(kesto <=30)
			return 13.50;
		else
			return 0.00;
	}

}
