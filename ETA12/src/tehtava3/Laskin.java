package tehtava3;

import java.util.Scanner;

public class Laskin {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Integer luku1;
		Integer luku2;
		while(true)
		{
			System.out.print("Anna 1. kokonaisluku: ");
			String intake = input.nextLine();
			luku1 = onKokonaisluku(intake);
			if(luku1 != null)
				break;
			else
				System.out.println("Virheellinen sy�te.");
		}
		while(true)
		{
			System.out.print("Anna 2. kokonaisluku: ");
			String intake = input.nextLine();
			luku2 = onKokonaisluku(intake);
			if(luku2 != null)
				break;
			else
				System.out.println("Virheellinen sy�te.");
		}
		input.close();
		System.out.println(luku1.toString() + " * " + luku2.toString() + " = " + kertolasku(luku1,luku2));
		System.out.println(luku1.toString() + " + " + luku2.toString() + " = " + summa(luku1,luku2));



	}
	
	private static Integer onKokonaisluku(String arvo)
	{
		Integer returnValue = null;
		try
		{
			returnValue = Integer.parseInt(arvo);
		}
		catch(NumberFormatException nfe)
		{
			returnValue = null;
		}
		
		return returnValue;
	}
	private static Integer kertolasku(int first, int second)
	{
		return first*second;
	}
	
	private static Integer summa(int first, int second)
	{
		return first+second;
	}

}



