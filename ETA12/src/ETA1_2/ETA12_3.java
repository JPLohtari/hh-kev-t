package ETA1_2;
import java.text.DecimalFormat;
import java.util.Scanner;

//Also known as Matka

public class ETA12_3 {

	public static void main(String[] args) {

		Scanner intake = new Scanner(System.in);
		System.out.print("Anna matka: ");
		Integer distance = intake.nextInt();
		System.out.print("Anna nopeus: ");
		Integer speed = intake.nextInt();
		Double time = (double) distance/speed;
		DecimalFormat format = new DecimalFormat("0.00");
		intake.close();

		System.out.print("Aikaa menee " + format.format(time) + " tuntia");
	}

}
