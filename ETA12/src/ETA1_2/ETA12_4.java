package ETA1_2;
import java.text.DecimalFormat;
import java.util.Scanner;

// Also known as Alennus

public class ETA12_4 {

	public static void main(String[] args) {

		DecimalFormat format = new DecimalFormat("0.00");
		
		Scanner intake = new Scanner(System.in);
		System.out.print("Anna alentamaton hinta: ");
		Double normprice = intake.nextDouble();
		System.out.print("Anna alennusprosentti: ");
		Integer rabatt = intake.nextInt();
		intake.close();
	
		Double curprice = normprice * ((100-(double) rabatt)/100);
		
		System.out.println("Alennettu hinta on " + format.format(curprice));
	}

}
