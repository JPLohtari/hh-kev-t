package ETA1_2;
import java.util.Scanner;

//Also known as Päiväsakko


public class ETA12_1 {

	public static void main(String[] args) {
		Integer mnthincome = 0;
		Scanner intake = new Scanner(System.in);
		System.out.print("Kuukausitulot: ");
		mnthincome = intake.nextInt();
		Integer fine = (mnthincome-255)/60;
		intake.close();
		
		System.out.println("Tuloilla " + mnthincome.toString() + " sakko on " + fine.toString());
	}

}
