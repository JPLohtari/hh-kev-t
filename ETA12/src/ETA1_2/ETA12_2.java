package ETA1_2;
import java.text.DecimalFormat;
import java.util.Scanner;

//Also known as Bensa


public class ETA12_2 {

	public static void main(String[] args) {
		DecimalFormat dec = new DecimalFormat("0.00");
		Scanner intake = new Scanner(System.in);
		System.out.print("Anna tankattu määrä: ");
		Double fuelIn = intake.nextDouble();
		System.out.print("Anna ajetut kilometrit: ");
		Integer klicks = intake.nextInt();
		System.out.print("Anna litrahinta: ");
		Double fuelPrice = intake.nextDouble();
		intake.close();
	
		Double cost = (fuelIn/klicks)*fuelPrice;
		System.out.println("Ajo per kilometri maksaa " + dec.format(cost));
	}

}