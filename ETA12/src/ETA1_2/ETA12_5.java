package ETA1_2;
import java.text.DecimalFormat;
import java.util.Scanner;

public class ETA12_5 {
//Also known as kimppakyyti
	
	
	public static void main(String[] args) {

		DecimalFormat format = new DecimalFormat("0.00");
		Scanner intake = new Scanner(System.in);
		System.out.print("Anna ajetut kilometrit: ");
		Integer klicks = intake.nextInt();
		System.out.print("Anna kulutus per 100 km: ");
		Double consump = intake.nextDouble();
		System.out.print("Anna polttoaineen litrahinta: ");
		Double fuelPrice = intake.nextDouble();
		System.out.print("Anna kimppakyytiläisten lukumäärä: ");
		Integer participants = intake.nextInt();
		intake.close();

		Double partCost = (double) klicks * (consump/100) * fuelPrice / (double) participants;
		System.out.println("Bensakustannus per henkilö on " + format.format(partCost) + " euroa");
	}

}