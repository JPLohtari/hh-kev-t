package ETA8;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class ETA8_3 {

	public static void main(String[] args) {
		// Date pvm = new Date();
		Date pvm = new Date((2015-1900),(3-1),26,0,0,0);
		String showDate = new SimpleDateFormat("yyyy-MM-dd").format(pvm);
		System.out.println("Tänään on "+showDate);
		if(pvm.getHours() > 11)
			System.out.println("On iltapäivä.");
		else
			System.out.println("On aamupäivä");
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Anna syntymävuotesi: ");
		Integer byear = scan.nextInt();
		Integer years = (pvm.getYear()+1900) - byear;
		System.out.println("Täytät/Olet täyttänyt tänä vuonna " + years.toString());
		System.out.print("Anna päivämäärä muodossa pp/kk/vvvv: ");
		String dateString = scan.next();
		String[] date = dateString.split("/");
		Date pvm2 = null;
		try {
			pvm2 = new Date((Integer.parseInt(date[2])-1900),(Integer.parseInt(date[1]) - 1),(Integer.parseInt(date[0])));
		}
		catch(Exception e)
		{
			System.out.println("Virhe päivämäärän syötössä");
		}
		if(pvm2 != null)
			System.out.println(pvm2.toString());
		
	}

}
