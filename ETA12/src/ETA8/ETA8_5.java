package ETA8;

import java.util.Scanner;

public class ETA8_5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Henkilo person = null;
		while(true)
		{
			System.out.println("1. Anna henkilö");
			System.out.println("2. Näytä henkilön tiedot");
			System.out.println("3. Muuta henkilön tietoja");
			System.out.println("0. Lopetus");
			System.out.print("Anna valintasi (0-3): ");
			String input = scan.nextLine();
			if(input.equals("0"))
				break;
			
			if(person == null && (input.equals("2") || input.equals("3")))
				System.out.println("Henkilöä ei ole");
			
			if(input.equals("1"))
				person = setPersonInfo();
			else if(input.equals("2") && person != null)
				System.out.println(person.toString());
			else if(input.equals("3") && person != null)
				person = setPersonInfo();
			
			System.out.println(" ");
		}
		return;

	}
	
	
	private static Henkilo setPersonInfo()
	{
		Scanner scan2 = new Scanner(System.in);
		Henkilo person = new Henkilo();
		System.out.print("Anna nimi: ");
		person.setName(scan2.nextLine());
		System.out.print("Anna osoite: ");
		person.setAddr(scan2.nextLine());
		
		return person;		
	}
}

class Henkilo {
	
	private String name;
	private String addr;
	
	Henkilo() {
		
	}
	
	Henkilo(String name, String addr)
	{
		this.name = name;
		this.addr = addr;
	}
	
	public String toString()
	{
		return "nimi="+this.name+", osoite="+this.addr;
	}
	
	public String getAddr() 
	{
		return this.addr;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddr(String addr)
	{
		this.addr = addr;
	}
	
}