package ETA8;

import java.util.Date;
import java.util.Scanner;

public class ETA8_4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Auto car = new Auto();
		System.out.print("Anna merkki: ");
		car.setMake(scan.nextLine());
		System.out.print("Anna malli: ");
		car.setModel(scan.nextLine());
		System.out.print("Anna rekisterinumero: ");
		car.setRegNum(scan.nextLine());
		System.out.print("Anna vuosimalli: ");
		car.setYear(scan.nextLine());
		
		System.out.println("Merkki ja malli: " + car.getMake() + " " + car.getModel());
		System.out.println("Rekisterinumero: " + car.getRegNum());
		System.out.println("Vuosimalli: " + car.getYearString());

	}

}

class Auto 
{
	private String make;
	private String model;
	private String regNum;
	private Integer year;
	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRegNum() {
		return regNum;
	}
	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}
	public Integer getYear() {
		return year;
	}
	public String getYearString() {
		return year.toString();
	}
	public void setYear(String year) {
		this.year = Integer.parseInt(year);
	}
	
	public Integer getCarAge() 
	{
		Date curDate = new Date();
		Integer curYear = curDate.getYear()+1900;
		return (curYear - this.year);
		
	}
}
