package ETA8;

import java.util.Scanner;

public class ETA8_1 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Anna nimi: ");
		String name = scan.nextLine();
		System.out.print("Anna osoite: ");
		String addr = scan.nextLine();
		Henkilo henkilo2 = new Henkilo(name, addr);
		System.out.println(henkilo2.toString());

	}
	
}

class Henkilo {
	
	private String name;
	private String addr;
	
	Henkilo() {
		
	}
	
	Henkilo(String name, String addr)
	{
		this.name = name;
		this.addr = addr;
	}
	
	public String toString()
	{
		return "nimi="+this.name+", osoite="+this.addr;
	}
	
	public String getAddr() 
	{
		return this.addr;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddr(String addr)
	{
		this.addr = addr;
	}
	
}
