package ETA8;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ETA8_2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		System.out.print("Anna pituus metreinä: ");
		Double height = scan.nextDouble();
		System.out.print("Anna paino kiloina: ");
		Double weight = scan.nextDouble();
		Koko bmi = new Koko(height, weight);
		System.out.println("Pituus: "+dec.format(bmi.getHeight()));
		System.out.println("Paino: "+bmi.getWeight().intValue());
		System.out.println("Painoindeksi: "+dec.format(bmi.getBmi()));

	}

}

class Koko
{
	private Double height;
	private Double weight;

	Koko() {}
	Koko(Double height, Double weight)
	{
		this.setHeight(height);
		this.setWeight(weight);
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	
	public Double getBmi() {
		return weight/(height*height);
	}
	
}
