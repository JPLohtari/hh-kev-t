package ETA10;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Scanner;

public class ETA10_1 {

	public static void main(String[] args) {
		DecimalFormat dec = new DecimalFormat("0.00");
		Scanner scan = new Scanner(System.in);
		Auto auto = new Auto();
		Henkilo henkilo = new Henkilo();
		System.out.print("Anna merkki: ");
		auto.setMake(scan.nextLine());
		System.out.print("Anna malli: ");
		auto.setModel(scan.nextLine());
		System.out.print("Anna rekisterinumero: ");
		auto.setRegNum(scan.nextLine());
		System.out.print("Anna vuosimalli: ");
		auto.setYear(scan.nextLine());
		System.out.print("Anna nimi: ");
		henkilo.setName(scan.nextLine());
		System.out.print("Anna osoite: ");
		henkilo.setAddr(scan.nextLine());
		System.out.print("Anna pituus metreinä: ");
		henkilo.getKoko().setHeight(scan.nextDouble());
		System.out.print("Anna paino kiloina: ");
		henkilo.getKoko().setWeight(scan.nextDouble());

		System.out.println("Nimi: " + henkilo.getName());
		System.out.println("Osoite: " + henkilo.getAddr());
		System.out.println("Auto: " + auto.getMakeModel());
		System.out.println("Pituus: " + dec.format(henkilo.getKoko().getHeight()));
		System.out.println("Painoindeksi: " + dec.format(henkilo.getKoko().getBmi()));
	}

}
class Henkilo {
	
	private String name;
	private String addr;
	private Koko koko;
	
	Henkilo() {
		koko = new Koko();
	}
	
	Henkilo(String name, String addr)
	{
		this.name = name;
		this.addr = addr;
	}
	Henkilo(String name, String addr, Koko koko)
	{
		this.name = name;
		this.addr = addr;
		this.koko = koko;
	}
	
	public String toString()
	{
		return "nimi="+this.name+", osoite="+this.addr+", bmi= "+koko.getBmi().toString();
	}
	
	public String getAddr() 
	{
		return this.addr;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddr(String addr)
	{
		this.addr = addr;
	}

	public Koko getKoko() {
		return koko;
	}

	public void setKoko(Koko koko) {
		this.koko = koko;
	}
	
}

class Koko
{
	private Double height;
	private Double weight;

	Koko() {}
	Koko(Double height, Double weight)
	{
		this.setHeight(height);
		this.setWeight(weight);
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	
	public Double getBmi() {
		return weight/(height*height);
	}
	
}

class Auto 
{
	private String make;
	private String model;
	private String regNum;
	private Integer year;
	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRegNum() {
		return regNum;
	}
	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}
	public Integer getYear() {
		return year;
	}
	public String getYearString() {
		return year.toString();
	}
	public void setYear(String year) {
		try {
			this.year = Integer.parseInt(year);
		}
		catch(Exception e)
		{
			this.year = 0;
		}
	}
	
	public Integer getCarAge() 
	{
		Date curDate = new Date();
		Integer curYear = curDate.getYear()+1900;
		return (curYear - this.year);
		
	}
	public String getMakeModel()
	{
		return this.getMake() + " " + this.getModel();
	}
}
