package ETA10;

import java.util.Date;
import java.util.Scanner;


public class ETA10_2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Opiskelija opiskelija = new Opiskelija();
		System.out.print("Anna opiskelijanumero: ");
		opiskelija.setNumero(scan.nextLine());
		System.out.print("Anna opiskelijan nimi: ");
		opiskelija.setNimi(scan.nextLine());
		System.out.print("Anna opiskelijan kotikunta: "); // This was typoed in viope
		opiskelija.setKotikunta(scan.nextLine());
		System.out.print("Anna tutkinnon nimi: "); // This was typoed in viope.
		opiskelija.getTutkinto().setTutkintoNimi(scan.nextLine());
		System.out.print("Anna tutkinnon suuntaus: ");
		opiskelija.getTutkinto().setSuuntaus(scan.nextLine());
		System.out.print("Anna tutkinnon tavoitteena oleva valmistumisvuosi: ");
		opiskelija.getTutkinto().setTavoiteVuosiAsString(scan.nextLine());
		System.out.println("Opiskelija " + opiskelija.getNimi() + " suorittaa tutkintoa " + opiskelija.getTutkinto().getTutkintoNimi());

	}

}

class Tutkinto
{
	private String tutkintoNimi;
	private String suuntaus;
	private Integer tavoiteVuosi;
	
	Tutkinto()
	{
		Date date = new Date();
		setTavoiteVuosi(date.getYear() + 1900 + 3);
	}
	
	Tutkinto(String tutkintoNimi, String suuntaus, Integer tavoiteVuosi)
	{
		this.setTutkintoNimi(tutkintoNimi);
		this.setSuuntaus(suuntaus);
		this.setTavoiteVuosi(tavoiteVuosi);
	}


	public String getTutkintoNimi() {
		return tutkintoNimi;
	}

	public void setTutkintoNimi(String tutkintoNimi) {
		this.tutkintoNimi = tutkintoNimi;
	}

	public Integer getTavoiteVuosi() {
		return tavoiteVuosi;
	}

	public void setTavoiteVuosi(Integer tavoiteVuosi) {
		this.tavoiteVuosi = tavoiteVuosi;
	}
	
	public void setTavoiteVuosiAsString(String tavoiteVuosi)
	{
		try {
			this.tavoiteVuosi = Integer.parseInt(tavoiteVuosi);
		}
		catch(Exception e)
		{
			Date date = new Date();
			this.tavoiteVuosi = date.getYear() + 1900 + 3;
		}
	}

	public String getSuuntaus() {
		return suuntaus;
	}

	public void setSuuntaus(String suuntaus) {
		this.suuntaus = suuntaus;
	}
	
	
}

class Opiskelija
{
	private String numero;
	private String nimi;
	private String kotikunta;
	private Tutkinto tutkinto;
	
	Opiskelija()
	{
		this.tutkinto = new Tutkinto();
	}
	
	Opiskelija(String numero, String nimi, String kotikunta)
	{
		this.setNumero(numero);
		this.setNimi(nimi);
		this.setKotikunta(kotikunta);
		this.tutkinto = new Tutkinto();
	}
	
	Opiskelija(String numero, String nimi, String kotikunta, Tutkinto tutkinto)
	{
		this.setNumero(numero);
		this.setNimi(nimi);
		this.setKotikunta(kotikunta);
		this.tutkinto = tutkinto;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getKotikunta() {
		return kotikunta;
	}

	public void setKotikunta(String kotikunta) {
		this.kotikunta = kotikunta;
	}
	
	public void setTutkinto(Tutkinto tutkinto)
	{
		this.tutkinto = tutkinto;
	}
	
	public Tutkinto getTutkinto()
	{
		return this.tutkinto;
	}
}