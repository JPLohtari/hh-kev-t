package ETA6;

import java.util.ArrayList;
import java.util.Scanner;
public class ETA6_2 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		ArrayList<String> regs = new ArrayList<String>();
		for(Integer i = 1; i <= 10; i++)
		{
			System.out.print("Anna rekisterinumero (- lopettaa): ");
			String input = scan.nextLine();
			if(input.equals("-"))
			{
				i=11;
				break;
			}
			
			if(isRegistration(input.toUpperCase()))
				regs.add(input.toUpperCase());
		}
		System.out.println("Rekisterinumerot ovat:");
		if(!regs.isEmpty())
		{
			for(int i = 0; i < regs.size(); i++)
			{
				System.out.println(regs.get(i));
			}
		}

	}

	private static Boolean isRegistration(String reg)
	{
		return reg.matches("[A-ZÖÅÄ]{1,3}-\\d{1,3}");
	}
}
