package ETA6;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class ETA6_1 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		ArrayList<Integer> temps = new ArrayList<Integer>();
		do
		{
			System.out.print("Anna lämpötila: ");
			Integer temp = scan.nextInt();
			if(temp.equals(-999))
				break;
			else
				temps.add(temp);
			
		}
		while(true);
		scan.close();
		Collections.sort(temps);
		System.out.println("Annoit lämpötilat: " + generateTempString(temps));
	}
	
	
	private static String generateTempString(ArrayList<Integer> temps) 
	{
		String output = "";
		for(int i = 0; i < temps.size(); i++)
		{
			output = output + temps.get(i).toString();
			if(i < (temps.size()-1))
				output = output + " ";
		}
		
		return output;
	}

}
