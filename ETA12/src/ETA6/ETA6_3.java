package ETA6;

import java.text.DecimalFormat;
import java.util.Scanner;
public class ETA6_3 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		
		System.out.print("Anna tuntipalkkasi: ");
		Double salary = scan.nextDouble();
		
		System.out.print("Monenko päivän tuntimäärät annat: ");
		Integer days = scan.nextInt();
		Integer[] hours = new Integer[days];
		Integer hoursSum = 0;
		String hoursString = "";
		
		for(int i = 0; i < days; i++)
		{
			System.out.print("Anna tuntien määrä päivässä: ");
			hours[i] = scan.nextInt();
			hoursSum = hoursSum + hours[i];
			hoursString = hoursString + hours[i].toString();
			if(i!=(days-1))
				hoursString = hoursString + " ";
		}
		
		System.out.println("Tunteja yhteensä: " + hoursSum.toString());
		System.out.println("Bruttopalkkasi: " + dec.format((double)hoursSum*salary) );
		System.out.println("Annoit tunnit: " + hoursString) ;
	}

}
