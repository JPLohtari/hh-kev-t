package ETA6;
import java.util.Scanner;
public class ETA6_4 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.print("Anna vesimittarin alkulukema: ");
		Integer start = scan.nextInt();
		
		Integer[] months = new Integer[6];
		for(Integer i = 0; i <6; i++)
		{
			Integer month = i+1;
			System.out.print("Anna "+month.toString()+". vesimittarin lukema: ");
			months[i] = scan.nextInt();
		}
		
		for(Integer i = 0; i < 6; i++)
		{
			Integer month = i+1;
			Integer diff = months[i]-start;
			start = months[i];
			System.out.println("Kuukauden "+month.toString()+" kulutus oli "+ diff.toString());
		}
		

	}

}
