package ETA6;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;
public class ETA6_5 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("0.00");
		
		System.out.print("Anna pelaajien lukumäärä: ");
		Integer players = scan.nextInt();
		
		Integer[] playerScore = new Integer[players];
		Integer scoreSum = 0;
		
		for(Integer i = 0; i < players; i++)
		{
			Integer player = i+1;
			System.out.print("Anna pelaajan " + player.toString() + ". tulos: ");
			playerScore[i] = scan.nextInt();
			scoreSum = scoreSum + playerScore[i];
		}
		
		Arrays.sort(playerScore);
		Double avg = scoreSum/(double)players;
		Integer playersBelowAvg = 0;
		for(int i = 0; i < players; i++)
		{
			if(playerScore[i] < avg)
				playersBelowAvg++;
		}
		
		scan.close();
		System.out.println("Pelien keskiarvo oli "+dec.format(avg));
		System.out.println("Pienin peli oli "+playerScore[0].toString());
		System.out.println("Suurin peli oli "+playerScore[players-1].toString());
		System.out.println(playersBelowAvg.toString() + " pelaajalla oli keskiarvoa pienempi tulos");

		
	}

}
