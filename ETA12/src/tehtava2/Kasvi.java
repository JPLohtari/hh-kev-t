package tehtava2;

public class Kasvi {
	private String nimi;
	private String heimo;
	private Integer korkeus;
	
	public Kasvi()
	{
		super();
		this.nimi = "";
		this.heimo= "";
		this.korkeus = 0;
	}
	
	public Kasvi(String nimi, String heimo, Integer korkeus) {
		super();
		this.nimi = nimi;
		this.heimo = heimo;
		this.korkeus = korkeus;
	}
	
	public String getNimi() {
		return nimi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getHeimo() {
		return heimo;
	}
	public void setHeimo(String heimo) {
		this.heimo = heimo;
	}
	public Integer getKorkeus() {
		return korkeus;
	}
	public void setKorkeus(Integer korkeus) {
		this.korkeus = korkeus;
	}
	
	@Override
	public String toString() {
		return "Kasvi [nimi = " + nimi + ", heimo = " + heimo + ", korkeus = "
				+ korkeus + "cm]";
	}
	
	

}
