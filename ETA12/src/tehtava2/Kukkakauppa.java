package tehtava2;

import java.util.ArrayList;

public class Kukkakauppa {

	public static void main(String[] args) {
		ArrayList<Kasvi> kasvit = new ArrayList<Kasvi>();
		kasvit.add(new Kasvi("Päivänkakkara","Kukkakasvi",1));
		kasvit.add(new Kasvi("Isomaksaruoho","Maksaruohokasvi",4));
		kasvit.add(new Kasvi("Ruusujuuri","Kasvi",7));
		
		System.out.println("Kasvinohjelma loi seuraavat kasvit:");
		for(int i = 0; i < kasvit.size(); i++)
		{
			System.out.println(kasvit.get(i).toString());
		}
	}

}
