package kanta1;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OppilasDAO {
	
	private Connection connect() throws SQLException, Exception {
		Connection dbConnection = null;
		String JDBCDriver = "org.mariadb.jdbc.Driver";
		String dbUrl = "jdbc:mariadb://localhost:3306/a1503083";
		
		try {
			Class.forName(JDBCDriver).newInstance();
			dbConnection = DriverManager.getConnection(dbUrl, "a1503083", "xuTEoO99m");
		}
		catch(SQLException se)
		{
			System.err.println("Kanta ei auennut. " + dbUrl + "\n" + se.getMessage() + "\n" + se.getStackTrace());
			throw se;
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage() + "\n" + e.getStackTrace());
			throw e;
		}
		return dbConnection;
	}
	
	private Oppilas teeOppilas(ResultSet resultSet) throws SQLException, Exception
	{
		Oppilas oppilas = null;
		String oppilasnro,etunimi,sukunimi,lahiosoite,postinro;
		Date syntpvm = new Date(0);
		if(resultSet != null)
		{
			try
			{
				oppilasnro = resultSet.getString("oppilasnro");
				etunimi = resultSet.getString("etunimi");
				sukunimi = resultSet.getString("sukunimi");
				syntpvm = resultSet.getDate("syntpvm");
				lahiosoite = resultSet.getString("lahiosoite");
				postinro = resultSet.getString("postinro");
				oppilas = new Oppilas(oppilasnro,etunimi,sukunimi,syntpvm,lahiosoite,postinro);
			}
			catch(SQLException se)
			{
				throw se;
			}
			catch(Exception e)
			{
				throw e;
			}
		}
		return oppilas;
		
	}
	
	public Oppilas haeOppilas(String key) throws SQLException, Exception
	{
		Oppilas oppilas = null;
		String sqlString = "SELECT oppilasnro,etunimi,sukunimi,syntpvm,lahiosoite,postinro FROM oppilas WHERE oppilasnro=?";
		PreparedStatement sqlStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		
		try {
			conn = connect();
			if(conn != null)
			{
				sqlStatement = conn.prepareStatement(sqlString);
				sqlStatement.setString(1, key);
				resultSet = sqlStatement.executeQuery();
				if(resultSet != null && resultSet.next())
				{
					oppilas = teeOppilas(resultSet);
				}
				else
				{
					conn.close();
					oppilas = null;
				}
			}
		}
		catch(SQLException se)
		{
			System.err.println("SQL-virhe" + se.getMessage());
			throw se;
		}
		catch(Exception e)
		{
			System.err.println("Muu virhe");
			throw e;
		}
		finally
		{
			if(conn != null && !conn.isClosed())
			{
				try
				{
					conn.close();
				}
				catch(Exception e)
				{
					throw e;
				}
			}
		}
		return oppilas;
	}
	
	public ArrayList<Oppilas> haeOppilaat() throws SQLException, Exception
	{
		ArrayList<Oppilas> oppilaat = new ArrayList<Oppilas>();
		
		String sqlString = "SELECT oppilasnro,etunimi,sukunimi,syntpvm,lahiosoite,postinro FROM oppilas";
		PreparedStatement sqlStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		
		try{
			conn = connect();
			if(conn != null)
			{
				sqlStatement = conn.prepareStatement(sqlString);
				resultSet = sqlStatement.executeQuery();
				if(resultSet != null)
				{
					while(resultSet.next())
					{
						oppilaat.add(teeOppilas(resultSet));
					}					
				}
				
				conn.close();
				resultSet.close();
			}
		}
		catch(SQLException se)
		{
			System.err.println("SQL-virhe");
			throw se;
		}
		catch(Exception e)
		{
			System.err.println("Muu virhe");
			throw e;
		}
		finally{
			if(conn != null && !conn.isClosed())
			{
				try {
					conn.close();
				}
				catch(Exception e)
				{
					throw e;
				}
			}
		}
		
		return oppilaat;	
	}

	public Boolean lisaaOppilas(Oppilas oppilas) throws SQLException, Exception
	{
		Boolean returnValue = false;
		
		String sqlString = "INSERT INTO oppilas(oppilasnro,etunimi,sukunimi,syntpvm,lahiosoite,postinro) VALUES(?,?,?,?,?,?)";
		PreparedStatement sqlStatement = null;
		Connection conn = null;
		
		try
		{
			conn = this.connect();
			if(conn != null)
			{
				sqlStatement = conn.prepareStatement(sqlString);
				sqlStatement.setString(1, oppilas.getOppilasnro());
				sqlStatement.setString(2, oppilas.getEtunimi());
				sqlStatement.setString(3, oppilas.getSukunimi());
				sqlStatement.setString(4, oppilas.getSyntpvm().toString());
				sqlStatement.setString(5, oppilas.getLahiosoite());
				sqlStatement.setString(6, oppilas.getPostinro());
				
				int sqlReturn = sqlStatement.executeUpdate();
				
				if(sqlReturn == 1)
					returnValue = true;
				
				conn.close();
			}
		}
		catch(SQLException se)
		{
			throw se;
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			if(conn != null && !conn.isClosed())
			{
				try
				{
					conn.close();
				}
				catch(Exception e)
				{
					throw e;
				}
			}
		}
		
		
		return returnValue;
	}
	

}
