package kanta1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class OppilaidenYllapito {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Boolean cont = true;
		while(cont)
		{
			int valinta = -1;
			System.out.println("1) Hae kaikki oppilaat");
			System.out.println("2) Hae tietty oppilas");
			System.out.println("3) Lisää oppilas");
			System.out.println("0) Lopeta");
			System.out.print("Valitse: ");
			valinta = input.nextInt();
			input.nextLine();
			
			switch(valinta)
			{
			case 1:
				haeOppilaat();
				break;
			case 2:
				haeOppilas(input);
				break;
			case 3:
				lisaaOppilas(input);
				break;
			case 0:
				cont = false;
				break;
			default:
				break;
			}
			
		}
	}
	
	private static void haeOppilaat()
	{
		OppilasDAO oppilasDAO = new OppilasDAO();
		try {
			ArrayList<Oppilas> oppilaat = oppilasDAO.haeOppilaat();
			for(int i = 0; i < oppilaat.size(); i++)
			{
				System.out.println(oppilaat.get(i).toString());
			}
		}
		catch(SQLException se)
		{
			System.err.println("SQL kuoli.");
		}
		catch(Exception e)
		{
			System.err.println("Yleinen Ongelma.");
		}
	}
	
	private static void haeOppilas(Scanner input)
	{
		OppilasDAO oppilasDAO = new OppilasDAO();
		Oppilas oppilas = null;
		System.out.print("Anna oppilaan opiskelijanumero: ");
		String key = input.nextLine();
		try {
			oppilas = oppilasDAO.haeOppilas(key);
		}
		catch(SQLException se)
		{
			System.err.println("SQL kuoli.");
		}
		catch(Exception e)
		{
			System.err.println("Ongelma.");
		}
		
		if(oppilas != null)
		{
			System.out.println(oppilas.toString());
		}
		else
		{
			System.out.println("Oppilasta ei löytynyt.");
		}
	}
	
	private static void lisaaOppilas(Scanner input)
	{	
		OppilasDAO oppilasDAO = new OppilasDAO();
		Oppilas oppilas = null;
		System.out.print("Anna oppilasnumero: ");
		String oppilasnumero = input.nextLine();
		try {
			oppilas = oppilasDAO.haeOppilas(oppilasnumero);
		}
		catch(SQLException se)
		{
			System.err.println("SQL kuoli.");
		}
		catch(Exception e)
		{
			System.err.println("Ongelma.");
		}
		
		if(oppilas != null)
		{
			System.out.println("Oppilas " + oppilasnumero + "  on jo tietokannassa!");
			return;
		}
		
		System.out.print("Anna etunimi: ");
		String etunimi = input.nextLine();
		System.out.print("Anna sukunimi: ");
		String sukunimi = input.nextLine();
		System.out.print("Anna syntymäpäivä (pp.kk.vvvv): ");
		String syntpvm = input.nextLine();
		System.out.print("Anna lähiosoite: ");
		String lahiosoite = input.nextLine();
		System.out.print("Anna postinumero: ");
		String postinro = input.nextLine();
		Boolean success = false;
		try {
			success = oppilasDAO.lisaaOppilas(new Oppilas(oppilasnumero,etunimi,sukunimi,syntpvm,lahiosoite,postinro));
		}
		catch(SQLException se)
		{
			System.err.println("SQL kuoli.");
		}
		catch(Exception e)
		{
			System.err.println("Ongelma.");
		}
		
		
	}

}
