package kanta1;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Oppilas {
	private String oppilasnro;
	private String etunimi;
	private String sukunimi;
	private Date syntpvm;
	private String lahiosoite;
	private String postinro;
	
	public Oppilas()
	{
		this.oppilasnro = this.etunimi = this.sukunimi = this.lahiosoite = this.postinro = "";
		java.util.Date date = new java.util.Date();
		this.syntpvm = new Date(date.getTime());
	}
	
	public Oppilas(String oppilasnro, String etunimi, String sukunimi, Date syntpvm, String lahiosoite,
			String postinro) {
		super();
		this.oppilasnro = oppilasnro;
		this.etunimi = etunimi;
		this.sukunimi = sukunimi;
		this.syntpvm = syntpvm;
		this.lahiosoite = lahiosoite;
		this.postinro = postinro;
	}
	
	public Oppilas(String oppilasnro, String etunimi, String sukunimi, String syntpvm, String lahiosoite,
			String postinro) {
		super();
		this.oppilasnro = oppilasnro;
		this.etunimi = etunimi;
		this.sukunimi = sukunimi;
		this.setSyntpvmByString(syntpvm);
		this.lahiosoite = lahiosoite;
		this.postinro = postinro;
	}
	
	@Override
	public String toString() {
		return "Oppilas [oppilasnro=" + oppilasnro + ", etunimi=" + etunimi + ", sukunimi=" + sukunimi + ", syntpvm="
				+ syntpvm + ", lahiosoite=" + lahiosoite + ", postinro=" + postinro + "]";
	}
	public String getOppilasnro() {
		return oppilasnro;
	}
	public void setOppilasnro(String oppilasnro) {
		this.oppilasnro = oppilasnro;
	}
	public String getEtunimi() {
		return etunimi;
	}
	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}
	public String getSukunimi() {
		return sukunimi;
	}
	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}
	public Date getSyntpvm() {
		return syntpvm;
	}
	public void setSyntpvm(Date syntpvm) {
		this.syntpvm = syntpvm;
	}
	public String getLahiosoite() {
		return lahiosoite;
	}
	public void setLahiosoite(String lahiosoite) {
		this.lahiosoite = lahiosoite;
	}
	public String getPostinro() {
		return postinro;
	}
	public void setPostinro(String postinro) {
		this.postinro = postinro;
	}
	public void setSyntpvmByString(String syntpvm)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		java.util.Date date = new java.util.Date();
		try {
			date = sdf.parse(syntpvm);
		} catch (ParseException e) {
			
		}
		finally
		{
			this.syntpvm = new Date(date.getTime());		}
	}
	
	
	
	
}
